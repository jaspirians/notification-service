<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src='lib/jquery-1.8.0.min.js' type='text/javascript'></script>
<script src='lib/jquery.cookie.js' type='text/javascript'></script>
<title>NexeGen API Login</title>

<script type="text/javascript">

	function login()
	{
		var username = document.getElementById('username').value;
		var password = document.getElementById('password').value;
		
	      $.ajax({
		        type : "POST",
		        url : "<%=request.getContextPath()%>/base/security/login",
		        async : false,
		        headers: {
		            "Authorization":"BASIC "+username+":"+password,
		            "Content-Type":"application/json"
		        },
		        data : {
								
				},
				success: function(data, textStatus, request){
					console.log(request.getResponseHeader('Etag'));
			        $.cookie("Etag", request.getResponseHeader('Etag'));
			        window.location = "api.jsp";
			        
			   },
			   error: function() {
					document.getElementById("invalidLogin").innerHTML = "Invalid login";
               } 
	      });
		
	}


</script>

</head>
<body>

	<label>Username</label>
	<input name="username" id="username" value="">
	<label>Password</label>
	<input name="password" id="password" value="">
	<button  onclick="login()">Login</button><br/>
	<div id="invalidLogin"></div>
</body>
</html>