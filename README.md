Env. Setup

1.	Install Eclipse here

2.	Install Java/JRE
3.	Install JCE Cryptography extensions here (Make sure that extensions match your java version)
4.	Install RabbitMQ server in local  here (for more info visit Message Queuing Setup )
5.	Install mysql.  You can install mysql in a number of ways. See installation instructions here.

         a.	Create database notifications in your mysql server. (CREATE DATABASE notifications)

         b.	Create user - nuser (CREATE USER 'nuser'@'localhost' IDENTIFIED BY 'aspire@123' )

         c.	Grant user access (GRANT ALL ON * . *  TO 'nuser'@'localhost' IDENTIFIED BY 'aspire@123' )

Source Code Setup
1.	You need to create an account on bitbucket or use your existing account.
2.	Get the code

        a.	Create a directory somewhere on your development box (i.e. ~/dev)

        b.	Go to that directory in the shell.

        c.	Checkout master branch -  git clone https://rajkumarmardubudi@bitbucket.org/jaspirians/notification-service.git

3.	Change to notification-service directory

4.	Run first build with ../tools/apache-maven-3.2.2/bin/mvn install. 
	
        .	maven might fail if it is behind a proxy to download or connect to maven central.  Instructions for proxy setup are here

5.	Once build in the shell works open your eclipse and point the Workspace to the platform  (i.e. ~/dev/notification-service) repository.

6.	Choose "Import Existing Projects Into Workspace" and once shown import all the component projects.

7.	Execute the build in Eclipse.