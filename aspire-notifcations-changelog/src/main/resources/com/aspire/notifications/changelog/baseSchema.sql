CREATE TABLE IF NOT EXISTS `apikey` (
  `id` varchar(64) NOT NULL,
  `app_name` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `mobile` varchar(64) DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `domain` varchar(512) NOT NULL,
  `quota` int(12) DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(36) DEFAULT NULL,
  `modified_by` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `provider` (
  `id` varchar(64) NOT NULL,
  `apikey_id` varchar(64) NOT NULL,
  `provider_type` varchar(64) NOT NULL,
  `provider_record` LONGTEXT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`apikey_id`) REFERENCES `apikey`(`id`) ON DELETE NO ACTION ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `email` (
  `id` varchar(64) NOT NULL,
  `provider_id` varchar(64) NOT NULL,
  `message` LONGTEXT,
  `subject` varchar(200) DEFAULT NULL,
  `sender_email` varchar(200) NOT NULL DEFAULT '',
  `receiver_email` varchar(200) NOT NULL DEFAULT '',
  `status` varchar(64) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`provider_id`) REFERENCES `provider`(`id`) ON DELETE NO ACTION ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `sms` (
  `id` varchar(64) NOT NULL,
  `provider_id` varchar(64) NOT NULL,
  `message` LONGTEXT,
  `from` varchar(64) NOT NULL DEFAULT '',
  `to` varchar(64) NOT NULL DEFAULT '',
  `messageId` varchar(64),
  `status` varchar(64) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`provider_id`) REFERENCES `provider`(`id`) ON DELETE NO ACTION ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `fcm` (
  `id` varchar(64) NOT NULL,
  `provider_id` varchar(64) NOT NULL,
  `data` LONGTEXT,
  `notification` LONGTEXT,
  `condition` varchar(200) NOT NULL DEFAULT '',
  `to` varchar(64) NOT NULL DEFAULT '',
  `status` varchar(64) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`provider_id`) REFERENCES `provider`(`id`) ON DELETE NO ACTION ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for batch `rule` */

CREATE TABLE IF NOT EXISTS `batch` (
  `id` varchar(64) NOT NULL,
  `name` varchar(1024) DEFAULT NULL,
  `batch_desc` varchar(1024) DEFAULT NULL,
  `batch_job_class` varchar(1024) DEFAULT NULL,
  `batch_handle` varchar(1024) DEFAULT NULL,
  `batch_trigger` varchar(1024) DEFAULT NULL,
  `batch_params` varchar(1024) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `batch_instance` */

CREATE TABLE IF NOT EXISTS `batch_instance` (
  `id` VARCHAR(64) NOT NULL,
  `statr_time` DATETIME DEFAULT NULL,
  `stop_time` DATETIME DEFAULT NULL,
  `next_time` DATETIME DEFAULT NULL,
  `error_message` LONGTEXT DEFAULT NULL,
  `status_code` VARCHAR(255) NOT NULL,
  `processed` INT(11) DEFAULT NULL,
  `failed` INT(11) DEFAULT NULL,
  `batch_id` VARCHAR(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;