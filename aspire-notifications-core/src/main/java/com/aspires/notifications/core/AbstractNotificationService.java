/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.core;

import com.aspires.notifications.exc.InvalidInputException;
import com.aspires.notifications.exc.NotificationException;
import com.aspires.notifications.resp.NotificationResponse;

import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

/**
 * Extended {@link NotificationService} interface
 * 
 * @author rajkumar.mardubudi
 * @see NotificationService
*/
public abstract class AbstractNotificationService<T, M> implements NotificationService<M> {
	
	protected String xsdFile;
	
	protected Class<ValidatedDocument> documentClass;
	
	protected Class objectFactory;
	
	protected T provider;
	
	protected AbstractNotificationService(String xsdFile, Class documentClass, Class objectFactory) {
		this.xsdFile = xsdFile;
		this.documentClass = documentClass;
		this.objectFactory = objectFactory;
	}

	/**
	 * @see NotificationService#buildConfiguration(String)
	 */
	@Override
	public void buildConfiguration(String inputXml) throws InvalidInputException {
		buildConfiguration(inputXml, this.xsdFile, documentClass.getClass(), objectFactory);
	}
	
	public void buildConfiguration(String inputXml, String xsdFile, Class documentClass, Class objFactory) throws InvalidInputException {

		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		JAXBElement<ValidatedDocument> object = null;
		
		URL xsdUrl = getClass().getClassLoader().getResource(xsdFile);
		try {
			schema = sf.newSchema(xsdUrl);
			JAXBContext jaxbContext = JAXBContext.newInstance(objFactory);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			unmarshaller.setSchema(schema);
			object = (JAXBElement<ValidatedDocument>) unmarshaller.unmarshal(getClass().getClassLoader().getResource(inputXml));			
		} catch (SAXException e) {
			e.printStackTrace();
		}catch(JAXBException e) {
			e.printStackTrace();
		}
		provider = (T)object.getValue();
	}
	
	public void buildConfiguration(String inputXml, Class documentClass) throws InvalidInputException {
	    Object object = JAXB.unmarshal(new ByteArrayInputStream(inputXml.getBytes()), documentClass);
	    provider = (T)object;
    }
	
	/**
	 * @see NotificationService#cancelMessage(String)
	 */
	@Override
	public void cancelMessage(String messageId) {
	}
	
	/**
	 * @see NotificationService#send(Object, NotificationType)
	 */
	@Override
	public String send(M message, NotificationType notificationType) throws NotificationException {
		return null;
	}
	
	/**
	 * @see NotificationService#send(Object, NotificationType, URL)
	 */
	@Override
	public String send(M message, NotificationType notificationType, URL callbackURL) throws NotificationException {
		return null;
	}
	
	/**
	 * @see NotificationService#checkStatus(String)
	 */
	@Override
	public NotificationResponse checkStatus(String messageId) {
		return null;
	}
	
	public T getProvider() {
		return provider;
	}

	public void setProvider(T provider) {
		this.provider = provider;
	}
	
	
}
