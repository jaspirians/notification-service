/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.core;

public interface DocumentValidator {
	
	public boolean validate(String schema);

}
