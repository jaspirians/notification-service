/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.core;

import com.aspires.notifications.exc.NotificationException;
import com.aspires.notifications.msg.Message;

public class NotificationMessage {
  private String from;
  private String to;
  private Message message;

  public void setFrom(String from) throws NotificationException {
    this.from = from;
  }
  
  public String getFrom(){
    return from;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public String getTo() {
    return to;
  }

  public void setMessage(Message message) {
    this.message = message;
  }

  public Message getMessage() {
    return message;
  }

}
