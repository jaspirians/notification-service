/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.core;

import com.aspires.notifications.exc.InvalidInputException;
import com.aspires.notifications.exc.NotificationException;
import com.aspires.notifications.resp.NotificationResponse;

import java.net.URL;

/**
 * This interface defines a strategy for sending, canceling, tracking messages
 * 
 * @author rajkumar.mardubudi
 * @see AbstractNotificationService
 */

public interface NotificationService<M> {

	/**
	 * 
	 * @param inputXml
	 * @throws InvalidInputException
	 */
	public void buildConfiguration(String inputXml) throws InvalidInputException;
	
	public void buildConfiguration(String inputXml, Class documentClass) throws InvalidInputException;
	
	/**
	 * Send the given message based Notification type
	 * @param message
	 * @param notificationType
	 * @return
	 * @throws NotificationException
	 */
	public String send(M message, NotificationType notificationType) throws NotificationException;

	/**
	 * Send the given message based Notification type
	 * @param message
	 * @param notificationType
	 * @param callbackURL
	 * @return
	 * @throws NotificationException
	 */
	public String send(M message, NotificationType notificationType, URL callbackURL) throws NotificationException;

	/**
	 * Check the notification status
	 * @param messageId
	 * @return
	 */
	public NotificationResponse checkStatus(String messageId);

	/**
	 * Cancel the message
	 * @param messageId
	 */
	public void cancelMessage(String messageId);

}
