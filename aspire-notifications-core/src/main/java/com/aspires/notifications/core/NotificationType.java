/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.core;

public enum NotificationType {
	String,
	Short,
	OTP
}
