/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.core;

/**
 * {@link NotificationsFactory}
 *  
 * @author rajkumar.mardubudi
 * @param <T>
 */
public final class NotificationsFactory<T> {

	/**
     * get the instance of the service
     */
	public static <T> T getService(Class<? extends NotificationService> service) {

		NotificationService<T> obj = null;
		try {
			obj = (NotificationService<T>) service.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch(IllegalAccessException e) {
			
		}
		return (T)obj;
	}

}
