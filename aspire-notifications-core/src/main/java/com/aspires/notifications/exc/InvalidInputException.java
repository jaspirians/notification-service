/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.exc;

/**
 * Base class for all mail exceptions.
 *
 * @author rajkumar.mardubudi
 */
@SuppressWarnings("serial")
public class InvalidInputException extends RuntimeException {

	/**
	 * Constructor for InvalidInputException.
	 * @param msg the detail message
	 */
	public InvalidInputException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for InvalidInputException.
	 * @param msg the detail message
	 * @param cause the root cause from the mail API in use
	 */
	public InvalidInputException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
