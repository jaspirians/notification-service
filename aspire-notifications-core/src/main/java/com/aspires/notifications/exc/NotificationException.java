/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.exc;

/**
 * Base class for all mail exceptions.
 *
 * @author rajkumar.mardubudi
 */
@SuppressWarnings("serial")
public abstract class NotificationException extends RuntimeException {

	/**
	 * Constructor for NotificationException.
	 * @param msg the detail message
	 */
	public NotificationException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for NotificationException.
	 * @param msg the detail message
	 * @param cause the root cause from the mail API in use
	 */
	public NotificationException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
