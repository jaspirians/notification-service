/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.msg;

public class DefaultMessage implements Message {
	
	private String message;
	
	public DefaultMessage(String str) {
		this.message = str;
	}

	public String getMessage() {
		return message;
	}

	
}
