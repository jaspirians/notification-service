/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.msg;

public interface Message {
	
	public String getMessage();

}
