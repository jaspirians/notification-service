/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.msg;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.StringResourceLoader;
import org.apache.velocity.runtime.resource.util.StringResourceRepository;
import org.junit.Assert;

import java.io.StringWriter;
import java.util.Map;

public class TemplatedMessage implements Message {
	
	private String templatePath = null;
	private Map<String, Object> valueMap = null;
	
	public TemplatedMessage(String templatePath, Map<String, Object> valueMap) {
		this.templatePath = templatePath;
		this.valueMap = valueMap;
	}

	public String getMessage() {
	   Assert.assertNotEquals(templatePath, "Path must not be null");
	    VelocityEngine engine = new VelocityEngine();
	    VelocityContext context = new VelocityContext(valueMap);
	    StringWriter result = new StringWriter();
	    Template template = engine.getTemplate(templatePath);
	    template.merge(context, result);
	     return result.toString();
	  }

	
}
