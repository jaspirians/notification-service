/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.otp;

/**
 * @author rajkumar.mardubudi
 * 
 */
public class OTP {
	public static String generate() {
		String key = "12345678";
		OTPProvider otpProvider = OTPProviderFactory.getOTPProvider(TOTPProvider.class);
		return otpProvider.generate("" + key, "" + System.currentTimeMillis(), 6);
	}
}
