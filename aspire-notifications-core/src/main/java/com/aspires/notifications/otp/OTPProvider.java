/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.otp;

import java.util.Map;

/**
 * @author rajkumar.mardubudi
 * 
 */
public interface OTPProvider {
	public String getName();

	public String generate(String key, String base, int digits);

}
