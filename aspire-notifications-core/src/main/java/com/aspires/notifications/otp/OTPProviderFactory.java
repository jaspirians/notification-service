/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.otp;

/**
 * @author rajkumar.mardubudi
 * 
 */
public class OTPProviderFactory<T> {

	public static <T> T getOTPProvider(Class<? extends OTPProvider> provider) {

		OTPProvider obj = null;
		try {
			obj = (OTPProvider) provider.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch(IllegalAccessException e) {
			
		}
		return (T)obj;
	}


}
