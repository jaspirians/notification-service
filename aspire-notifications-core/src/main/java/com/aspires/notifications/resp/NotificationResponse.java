/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspires.notifications.resp;

import com.aspires.notifications.core.NotificationMessage;

public class NotificationResponse {

	NotificationMessage message = null;
	String messageId = null;
	MessageStatus status;

	public enum MessageStatus {
		PENDING, SENT, CANCELLED, REJECTED;
	}

}
