package com.aspires.notifications.echo;

import com.aspires.notifications.core.AbstractNotificationService;
import com.aspires.notifications.core.NotificationMessage;
import com.aspires.notifications.core.NotificationType;
import com.aspires.notifications.exc.InvalidInputException;
import com.aspires.notifications.exc.NotificationException;

public class EchoNotificationService extends AbstractNotificationService {

	@Override
	public void buildConfiguration(String inputXml) throws InvalidInputException {
		// TODO Auto-generated method stub
		super.buildConfiguration(inputXml);
	}
	
	@Override
	public String send(Object message, NotificationType notificationType) throws NotificationException {
		// TODO Auto-generated method stub
		return super.send(message, notificationType);
	}
	
	public EchoNotificationService() {
		super("config/emailConfig.xsd", Object.class, Object.class);
	}
}
