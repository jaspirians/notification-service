/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.batch.handlers;

import com.aspire.commons.engine.exe.ServiceException;
import com.aspire.commons.entities.BatchInstance;

/**
 * Unit test pursose
 * @author rajkumar.mardubudi
 *
 */
public class TestBatchHandler implements BatchHandler
{
    @Override
    public void execute(BatchInstance instance) throws ServiceException
    {
        System.out.println("Test Batch Handler");
        
    }
}
