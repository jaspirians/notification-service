/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors;

import com.aspire.commons.engine.adaptors.core.ComplexException;
import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.adaptors.core.Status;
import com.aspire.commons.engine.adaptors.req.AdapterRequest;
import com.aspire.commons.engine.adaptors.req.Message;
import com.aspire.commons.engine.adaptors.req.NotificationAdapterRequest;
import com.aspire.commons.engine.adaptors.res.AdapterResponse;
import com.aspire.commons.engine.adaptors.res.NotificationAdapterResponse;
import com.aspire.commons.engine.services.EmailService;
import com.aspire.commons.engine.services.ServiceFactory;
import com.aspire.commons.engine.services.SmsService;
import com.aspire.commons.entities.Email;
import com.aspire.commons.entities.Sms;
import com.aspire.commons.logs.RLogFactory;
import com.aspire.commons.utils.MsgType;

import org.apache.commons.logging.Log;

import javax.mail.Address;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.oval.exception.ValidationFailedException;

/**
 * Defines an adapter that exposes management functions
 * 
 * @see Adapter
 * 
 * @author rajkumar.mardubudi
 */
@Api(value = "/services", description = "Provider Adapter")
@Path("/services")
public class NotificationAdapter extends AdapterBase
{
    protected static final Log log = RLogFactory.getLog(NotificationAdapter.class);
    protected static final MessageBundle bundle = new MessageBundle(
            NotificationAdapter.class);
    
    /**
     * Create new instance of Notification
     * 
     * @param req
     *            of {@link AdapterRequest} containing the incoming
     *            {@link Address}
     * @return {@link AdapterResponse} containing the newly created
     *         {@link Message}
     */
    @ApiOperation(value = "send", notes = "send a new message")
    @POST
    @Path("/send")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public NotificationAdapterResponse send(NotificationAdapterRequest req)
    {
        NotificationAdapterResponse response = new NotificationAdapterResponse();
        EmailService emailService = ServiceFactory.getEmailService();
        SmsService smsService = ServiceFactory.getSmsService();
        long tm = System.currentTimeMillis();
        try
        {
            super.validate(req, response);
            
            if (MsgType.EMAIL.toString().equals(req.getMsgType()))
            {
                Email email = emailService.save(req.getMessage().getEmail());
                response.setMessageId(email.getId());
            }
            else if (MsgType.SMS.toString().equals(req.getMsgType()))
            {
                Sms sms = smsService.save(req.getMessage().getSms());
                response.setMessageId(sms.getId());
            }
            else if (MsgType.FCM.toString().equals(req.getMsgType()))
            {
                // to-do
            }
            else
            {
                throw new ComplexException(new ComplexMessage(
                        "E_ValidationFailed", bundle, req.getClass()));
            }
            
        }
        catch (ValidationFailedException e)
        {
            response.setStatus(Status.REQUEST_VALIDATION_FAILED);
            log.error(e);
        }
        catch (ComplexException e)
        {
            log.error(e.getCompositeMessage(), e);
            response.getErrors().add(e.getCompositeMessage());
        }
        catch (Exception e)
        {
            ComplexMessage msg = new ComplexMessage("E_ProviderRegister");
            log.error(msg, e);
            response.getErrors().add(msg);
        }
        finally
        {
            response.setTime(System.currentTimeMillis() - tm);
        }
        response.setStatus(Status.OK);
        return response;
    }
  
}
