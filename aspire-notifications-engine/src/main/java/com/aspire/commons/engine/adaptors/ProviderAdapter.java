/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors;

import com.aspire.commons.engine.adaptors.core.ComplexException;
import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.adaptors.core.Status;
import com.aspire.commons.engine.adaptors.req.AdapterRequest;
import com.aspire.commons.engine.adaptors.req.ProviderAdapterRequest;
import com.aspire.commons.engine.adaptors.res.AdapterResponse;
import com.aspire.commons.engine.adaptors.res.ProviderAdapterResponse;
import com.aspire.commons.engine.services.ProviderService;
import com.aspire.commons.engine.services.ServiceFactory;
import com.aspire.commons.entities.Provider;
import com.aspire.commons.logs.RLogFactory;
import com.aspire.commons.utils.ProviderType;

import org.apache.commons.logging.Log;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

import javax.mail.Address;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.oval.exception.ValidationFailedException;

/**
 * Defines an adapter that exposes management functions
 * 
 * @see Adapter
 * 
 * @author rajkumar.mardubudi
 */
@Api(value = "/services", description = "Provider Adapter")
@Path("/services")
public class ProviderAdapter extends AdapterBase
{
    protected static final Log log = RLogFactory.getLog(ProviderAdapter.class);
    protected static final MessageBundle bundle = new MessageBundle(
            ProviderAdapter.class);
    
    /**
     * Create new instance of {@link Address} entity
     * 
     * @param req
     *            of {@link AdapterRequest} containing the incoming
     *            {@link Address}
     * @return {@link AdapterResponse} containing the newly created
     *         {@link Address}
     */
    @ApiOperation(value = "provider", notes = "Register new adapter eagainst existing implementation")
    @POST
    @Path("/provider/register")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ProviderAdapterResponse register(ProviderAdapterRequest req)
    {
        ProviderAdapterResponse response = new ProviderAdapterResponse();
        ProviderService service = ServiceFactory.getProviderService();
        String xmlFileName = req.getProvider().getApiKey().getId()+".xml";
        
        long tm = System.currentTimeMillis();
        try
        {
            
            super.validate(req, response);
            
            /*
            if (ProviderType.EMAIL.toString()
                    .equals(req.getProvider().getProviderType()))
            {
                URL xsdUrl = getClass().getClassLoader().getResource("config/emailConfig.xsd");
                validateAgainSchema(xsdUrl, req);
            }
            else if (ProviderType.TWILIO_SMS.toString()
                    .equals(req.getProvider().getProviderType()))
            {
                URL xsdUrl = getClass().getClassLoader().getResource("/aspire-notifications-sms-twilio/src/main/resources/config/twiloSmsConfig.xsd");
                validateAgainSchema(xsdUrl, req);
               
            }
            else if (ProviderType.CLICKTELL_SMS.toString()
                    .equals(req.getProvider().getProviderType()))
            {
                URL xsdUrl = getClass().getClassLoader().getResource("config/ClickTellSmsConfig.xsd");
                validateAgainSchema(xsdUrl, req);
            }
            else if (ProviderType.FCM.toString()
                    .equals(req.getProvider().getProviderType()))
            {
                URL xsdUrl = getClass().getClassLoader().getResource("config/FcmConfig.xsd");
                validateAgainSchema(xsdUrl, req);
            }
            */
            
            Provider provider = service.registerProvider(req.getProvider());
            provider.setApiKey(null);
            response.setProvider(provider);
            response.setStatus(Status.OK);
            return response;
        }
        catch (ValidationFailedException e)
        {
            response.setStatus(Status.REQUEST_VALIDATION_FAILED);
            log.error(e);
        }
        catch (ComplexException e)
        {
            log.error(e.getCompositeMessage(), e);
            response.getErrors().add(e.getCompositeMessage());
        }
        catch (Exception e)
        {
            ComplexMessage msg = new ComplexMessage("E_ProviderRegister");
            log.error(msg, e);
            response.getErrors().add(msg);
        }
        finally
        {
            File xmlFile = new File(xmlFileName);
            if(xmlFile.exists()) 
            {
                xmlFile.delete();
            }
            response.setTime(System.currentTimeMillis() - tm);
        }
        return response;
    }
    
    /**
    * Update instance of {@link Address} entity
    * @param req of {@link AdapterRequest} containing the incoming {@link Address}
    * @return {@link AdapterResponse} containing the newly created {@link Address}
    */
   @ApiOperation(value = "provider", notes = "Update an existing address")
   @PUT 
   @Path("/provider")
   @Consumes({MediaType.APPLICATION_JSON}) 
   @Produces({MediaType.APPLICATION_JSON}) 
   public ProviderAdapterResponse update(ProviderAdapterRequest req)
   {
       ProviderAdapterResponse response = new ProviderAdapterResponse();
      
       long tm = System.currentTimeMillis();
       try
       {
           ProviderService service = ServiceFactory.getProviderService();
           Provider provider = service.update(req.getProvider());
           provider.setApiKey(null);
           response.setProvider(provider);
           response.setStatus(Status.OK);
       }
       catch (ValidationFailedException e)
       {
           response.setStatus(Status.REQUEST_VALIDATION_FAILED);
           log.error(e);
       }
       catch (ComplexException e)
       {
           log.error(e.getCompositeMessage(), e);
           response.getErrors().add(e.getCompositeMessage());
       }
       catch (Exception e)
       {
           ComplexMessage msg = new ComplexMessage("E_ProviderCreate");
           log.error(msg, e);
           response.getErrors().add(msg);
       }
       finally
       {
           response.setTime(System.currentTimeMillis() - tm);
       }
       return response;
   }

    
    /**
     * Get specific provider information.
     * 
     * @param req
     *            of {@link AdapterRequest} containing the incoming
     *            {@link Address}
     * @return {@link AdapterResponse} containing the newly created
     *         {@link Address}
     */
    @ApiOperation(value = "/provider/{id}", notes = "Retrieve provider information.")
    @GET
    @Path("/provider/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public ProviderAdapterResponse get(@PathParam("id") String id)
    {
        ProviderAdapterResponse response = new ProviderAdapterResponse();
        long tm = System.currentTimeMillis();
        try
        {
            ProviderService service = ServiceFactory.getProviderService();
            Provider provider = service.getProviderById(id);
            provider.setApiKey(null);
            response.setProvider(provider);
            response.setStatus(Status.OK);
        }
        catch (ComplexException e)
        {
            log.error(e.getCompositeMessage(), e);
            response.getErrors().add(e.getCompositeMessage());
        }
        catch (Exception e)
        {
            ComplexMessage msg = new ComplexMessage("E_GetProvider");
            log.error(msg, e);
            response.getErrors().add(msg);
        }
        finally
        {
            response.setTime(System.currentTimeMillis() - tm);
        }
        return response;
    }
    
    
    /**
     * Delete instance of {@link Provider} entity
     * @param req of {@link AdapterRequest} containing the incoming {@link Provider}
     * @return {@link AdapterResponse} containing the newly created {@link Provider}
     */
    @ApiOperation(value = "/provider/{apikey}/{id}", notes = "Delete a existing profile")
    @DELETE 
    @Path("/provider/{apikey}/{id}") 
    @Produces({MediaType.APPLICATION_JSON}) 
    public ProviderAdapterResponse delete(@PathParam("apikey") String apikey, @PathParam("id") String id)
    {
        ProviderAdapterResponse response = new ProviderAdapterResponse();
        long tm = System.currentTimeMillis();
        try
        {
            ProviderService service = ServiceFactory.getProviderService();
            Provider provider = service.getProviderById(id);
            service.remove(provider);
            response.setStatus(Status.OK);  
        }
        catch (ComplexException e)
        {
            log.error(e.getCompositeMessage(), e);
            response.getErrors().add(e.getCompositeMessage());
        }
        catch (Exception e)
        {
            ComplexMessage msg = new ComplexMessage("E_ProviderDelete");
            log.error(msg, e);
            response.getErrors().add(msg);
        }
        finally
        {
            response.setTime(System.currentTimeMillis() - tm);
        }
        return response;
    }
    
    private void validateAgainSchema(URL xsdUrl, ProviderAdapterRequest req) throws SAXException, IOException
    {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(xsdUrl);
        Validator validator = schema.newValidator();
        FileWriter xmlFile = new FileWriter(req.getProvider().getApiKey().getId()+".xml");
        xmlFile.write(req.getProvider().getProviderRecord());
        xmlFile.close();
        validator.validate(new StreamSource(new File(req.getProvider().getApiKey().getId()+".xml")));
    }
}
