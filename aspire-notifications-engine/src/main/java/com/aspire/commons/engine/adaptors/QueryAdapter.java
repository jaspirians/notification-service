package com.aspire.commons.engine.adaptors;

import com.aspire.commons.engine.adaptors.core.ComplexException;
import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.Status;
import com.aspire.commons.engine.adaptors.req.QueryAdapterRequest;
import com.aspire.commons.engine.adaptors.res.QueryAdapterResponse;
import com.aspire.commons.engine.services.QueryService;
import com.aspire.commons.engine.services.ServiceFactory;
import com.aspire.commons.entities.Query;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.oval.exception.ValidationFailedException;

/**
 * Define query specific adapter.
 * 
 * @see Adapter
 */
@Api(value = "/services", description= "Query adapter" )
@Path("/services")
public class QueryAdapter extends AdapterBase
{
    
    /**
     * Create new instance of {@link Query} entity
     * 
     * @param request
     *            of {@link QueryAdapterRequest} containing the incoming
     *            {@link Query}
     * @return {@link QueryAdapterResponse} containing the records
     */
    @ApiOperation(value = "/query", notes = "Retreive records based on conditions from view")
    @POST
    @Path("/query")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public QueryAdapterResponse query(QueryAdapterRequest request)
    {
        QueryAdapterResponse response = new QueryAdapterResponse();
        long tm = System.currentTimeMillis();
        try
        {
            super.validate(request, response);
            QueryService service = ServiceFactory.getQueryService();
            List<Object[]> records = service.query(request.getQuery());
            response.setRecords(records);
            response.setStatus(Status.OK);
            return response;
        }
        catch (ValidationFailedException e)
        {
            response.setStatus(Status.REQUEST_VALIDATION_FAILED);
            log.error(e);
        }
        catch (ComplexException e)
        {
            log.error(e.getCompositeMessage(), e);
            response.getErrors().add(e.getCompositeMessage());
        }
        catch (Exception e)
        {
            ComplexMessage msg = new ComplexMessage("E_QueryFetch");
            log.error(msg, e);
            response.getErrors().add(msg);
        }
        finally
        {
            response.setTime(System.currentTimeMillis() - tm);
        }
        return response;
    }
    
}
