/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.adaptors.core;

import java.io.PrintStream;
import java.io.PrintWriter;

import javax.xml.bind.annotation.XmlTransient;

/**
 * {@link ComplexException} is a wrapped exception that uses
 * {@link ComplexMessage} utility to produce standard system exceptions.
 * 
 * @author rajkumar.mardubudi
 */
public class ComplexException extends RuntimeException
{
    protected ComplexMessage compositeMessage = null;
    /**
     * long BaseFormattedException.java
     */
    private static final long serialVersionUID = -3269397209932948665L;
    
    /**
     */
    public ComplexException(ComplexMessage msg, Throwable ex)
    {
        super(msg.format(), ex);
        compositeMessage = msg;
    }
    
    /**
     * @return the compositeMessage
     */
    public ComplexMessage getCompositeMessage()
    {
        return compositeMessage;
    }
    
    /**
     * @param compositeMessage
     *            the compositeMessage to set
     */
    public void setCompositeMessage(ComplexMessage compositeMessage)
    {
        this.compositeMessage = compositeMessage;
    }
    
    /**
     * @see java.lang.Throwable#fillInStackTrace()
     */
    @Override
    public synchronized Throwable fillInStackTrace()
    {
        return super.fillInStackTrace();
    }
    
    /**
     * @see java.lang.Throwable#getCause()
     */
    @Override
    public Throwable getCause()
    {
        return super.getCause();
    }
    
    /**
     * @see java.lang.Throwable#getLocalizedMessage()
     */
    @Override
    public String getLocalizedMessage()
    {
        return super.getLocalizedMessage();
    }
    
    /**
     * @see java.lang.Throwable#getMessage()
     */
    @Override
    public String getMessage()
    {
        return super.getMessage();
    }
    
    /**
     * @see java.lang.Throwable#initCause(java.lang.Throwable)
     */
    @Override
    public synchronized Throwable initCause(Throwable arg0)
    {
        return super.initCause(arg0);
    }
    
    /**
     * @see java.lang.Throwable#printStackTrace()
     */
    @Override
    public void printStackTrace()
    {
        super.printStackTrace();
    }
    
    /**
     * @see java.lang.Throwable#printStackTrace(java.io.PrintStream)
     */
    @Override
    public void printStackTrace(PrintStream arg0)
    {
        super.printStackTrace(arg0);
    }
    
    /**
     * @see java.lang.Throwable#printStackTrace(java.io.PrintWriter)
     */
    @Override
    public void printStackTrace(PrintWriter arg0)
    {
        super.printStackTrace(arg0);
    }
    
    /**
     * @see java.lang.Throwable#setStackTrace(java.lang.StackTraceElement[])
     */
    @Override
    public void setStackTrace(StackTraceElement[] arg0)
    {
        super.setStackTrace(arg0);
    }
    
    /**
     * @see java.lang.Throwable#toString()
     */
    @Override
    public String toString()
    {
        return super.toString();
    }
    
    /**
     * @see java.lang.Throwable#getStackTrace()
     */
    @Override
    @XmlTransient
    public StackTraceElement[] getStackTrace()
    {
        return super.getStackTrace();
    }
    
    /**
     */
    public ComplexException()
    {
    }
    
    /**
     */
    public ComplexException(ComplexMessage msg)
    {
        super(msg.format());
        compositeMessage = msg;
    }
    
    public String getId()
    {
        return compositeMessage.getId();
    }
    
}
