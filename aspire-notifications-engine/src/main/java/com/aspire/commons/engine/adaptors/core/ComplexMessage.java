/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.core;

import io.swagger.annotations.ApiModelProperty;

import java.text.MessageFormat;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * {@link ComplexMessage} is used to produce complete errors and messages within
 * product and will standardize error and logging. The message will be
 * compatible to JAXB marshaling
 * 
 * @author rajkumar.mardubudi
 */
@XmlRootElement
public class ComplexMessage
{
    private Object[] params;
    private String id = null;
    private MessageFormat format = null;
    
    /**
     * @return true if this {@link ComplexMessage} should be raising an alert
     */
    public boolean shouldRaise()
    {
        return (!StringUtils.isBlank(id)
                && (id.startsWith("E_") || id.startsWith("F_")));
    }
    
    /**
     * @return the params
     */
    @XmlTransient
    public Object[] getParams()
    {
        return params;
    }
    
    /**
     * @param params
     *            the params to set
     */
    public void setParams(Object[] params)
    {
        this.params = params;
    }
    
    /**
     * @return the format
     */
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public MessageFormat getFormat()
    {
        return format;
    }
    
    /**
     * @param format
     *            the format to set
     */
    public void setFormat(MessageFormat format)
    {
        this.format = format;
    }
    
    /**
     * String source formatter that formats specific resource
     * 
     * @param format
     * @param params
     */
    public ComplexMessage()
    {
    }
    
    /**
     * String source formatter that formats specific resource
     * 
     * @param format
     * @param params
     */
    public ComplexMessage(String id, Object... params)
    {
        this.params = params;
        this.id = id;
    }
    
    /**
     * String source formatter that formats specific resource
     * 
     * @param format
     * @param params
     */
    public ComplexMessage(String id, MessageFormat format, Object... params)
    {
        this.params = params;
        this.format = format;
        this.id = id;
    }
    
    /**
     * Shorthand for new {@link ComplexMessage}
     * 
     * @param name
     * @param bundle
     * @param params
     * @return
     */
    public static String toMessage(String name, MessageBundle bundle,
            Object... params)
    {
        ComplexMessage msg = new ComplexMessage(name, bundle, params);
        return msg.getMessage();
    }
    
    /**
     * @return the message
     */
    @XmlElement(name = "message")
    public String getMessage()
    {
        StringBuffer buf = new StringBuffer();
        if (format != null)
        {
            format.format(params, buf, null);
        }
        else
        {
            buf.append(this.id);
            buf.append("raw params = ");
            buf.append(this.params);
        }
        return buf.toString();
    }
    
    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message)
    {
    }
    
    /**
     * String source formatter that formats specific resource
     * 
     * @param format
     * @param params
     */
    public ComplexMessage(String id, MessageBundle bundle, Object... params)
    {
        this.params = params;
        this.format = bundle.getFormat(id);
        if (this.format == null)
        {
            if (ArrayUtils.isEmpty(params))
            {
                this.format = new MessageFormat("");
            }
            else
            {
                StringBuilder bb = new StringBuilder();
                for (int i = 0; i < params.length; i++)
                {
                    bb.append(" {").append(i).append("}");
                }
                this.format = new MessageFormat(bb.toString());
            }
        }
        this.id = id;
    }
    
    /**
     * @see java.lang.Object#toString()
     */
    /**
     * @return the id
     */
    @XmlElement(name = "code")
    public String getId()
    {
        return id;
    }
    
    /**
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String format(MessageFormat format)
    {
        Throwable t = new Throwable();
        t.fillInStackTrace();
        StackTraceElement[] se = t.getStackTrace();
        StringBuffer sb = new StringBuffer();
        sb.append(se[3].getClassName()).append(":")
                .append(se[3].getLineNumber()).append(" ");
        if (format == null)
        {
            sb.append(this.id == null ? "Unknown error" : this.id);
            if (params != null)
            {
                for (Object p : params)
                {
                    sb.append(" ").append(p.toString());
                }
            }
        }
        else
        {
            format.format(params, sb, null);
        }
        return sb.toString();
    }
    
    public String format()
    {
        return this.format(this.format);
    }
}
