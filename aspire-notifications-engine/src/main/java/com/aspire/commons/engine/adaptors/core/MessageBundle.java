/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.adaptors.core;

import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Loads and compiles resource bundles for platform usage.
 * 
 * @author rajkumar.mardubudi
 */
public class MessageBundle
{
    HashMap<String, MessageFormat> formats = new HashMap<String, MessageFormat>();
    
    /**
     * 
     */
    public MessageBundle(Class<?> clazz)
    {
        this(clazz, Locale.getDefault());
    }
    
    /**
     * 
     */
    public MessageBundle(Class<?> clazz, Locale locale)
    {
        String basePkgname = "com.aspire.notifications.resources";
        
        ResourceBundle rb = null;
        try
        {
            rb = ResourceBundle.getBundle(basePkgname + ".messages", locale);
            
        }
        catch (Exception e)
        {
            System.err.println(e);
        }
        if (rb == null)
        {
            return;
        }
        Enumeration<String> ex = rb.getKeys();
        while (ex.hasMoreElements())
        {
            String key = ex.nextElement();
            String val = rb.getString(key);
            formats.put(key, new MessageFormat(val));
        }
    }
    
    /**
     * @param id
     * @param params
     * @return
     */
    public String format(String id, Object... params)
    {
        MessageFormat f = this.getFormat(id);
        return f.format(params);
        
    }
    
    /**
     * @param id
     * @return
     */
    public MessageFormat getFormat(String id)
    {
        return this.formats.get(id);
    }
}
