/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.core;

import java.util.HashMap;

/**
 * Standard status codes
 * 
 * @author rajkumar.mardubudi
 * 
 */
public enum Status
{
    OK(0), GENERIC_FAILURE(1000), REQUEST_VALIDATION_FAILED(
            1001), UNRESOLVED_PROVIDER(1005), UNRESOLVED_PROVIDER_CONTEXT(
                    1006), PROVIDER_FAILED(1022), INVALID_EMAIL(1041);
    
    /**
     */
    private int code = 1;
    
    /**
     * @param code
     */
    private Status(int code)
    {
        this.code = code;
    }
    
    /**
     * From code conversion
     * 
     * @param code
     * @return
     */
    public static Status toStatus(int code)
    {
        for (Status c : Status.values())
        {
            if (c.code == code)
            {
                return c;
            }
        }
        return null;
    }
    
    public int code()
    {
        return this.code;
    }
}
