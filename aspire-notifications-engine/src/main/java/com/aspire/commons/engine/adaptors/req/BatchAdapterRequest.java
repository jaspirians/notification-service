/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.req;

import com.aspire.commons.entities.Batch;

import javax.xml.bind.annotation.XmlElement;

/**
 * @see AdapterRequest
 * @see XmlElement
 */
public class BatchAdapterRequest implements AdapterRequest
{
    
    private Batch batch;
    
    /**
     * @return the batch
     */
    @XmlElement(type = Batch.class)
    public Batch getBatch()
    {
        return batch;
    }
    
    /**
     * @param batch
     *            the batch to set
     */
    public void setBatch(Batch batch)
    {
        this.batch = batch;
    }
    
}
