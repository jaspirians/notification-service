/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.req;

import com.aspire.commons.entities.Email;
import com.aspire.commons.entities.Sms;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rajkumar.mardubudi
 *
 */
@XmlRootElement
public class Message
{
    private Email email;
    private Sms sms;
    
    /**
     * @return the email
     */
    public Email getEmail()
    {
        return email;
    }
    
    /**
     * @param email
     *            the email to set
     */
    public void setEmail(Email email)
    {
        this.email = email;
    }

    /**
     * @return the sms
     */
    public Sms getSms()
    {
        return sms;
    }

    /**
     * @param sms the sms to set
     */
    public void setSms(Sms sms)
    {
        this.sms = sms;
    }
    
}
