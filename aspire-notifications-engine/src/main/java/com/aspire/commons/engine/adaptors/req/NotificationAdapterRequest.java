/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.req;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

/**
 * @see AdapterRequest
 * @see XmlElement
 * 
 * @author rajkumar.mardubudi
 */
@XmlRootElement
public class NotificationAdapterRequest implements AdapterRequest
{
    
    
    @NotNull
    @NotEmpty
    //@Assert(expr="_value.msgType=='SMS' || _value.msgType=='EMAIL' || _value.msgType=='FCM'", lang = "javascript")
    private String msgType;
    private String apiKey;
    
    @NotNull
    @NotEmpty
    private Message message;
    
    /**
     * @return the msgType
     */
    public String getMsgType()
    {
        return msgType;
    }
    
    /**
     * @param msgType
     *            the msgType to set
     */
    public void setMsgType(String msgType)
    {
        this.msgType = msgType;
    }
    
    /**
     * @return the apiKey
     */
    public String getApiKey()
    {
        return apiKey;
    }
    
    /**
     * @param apiKey
     *            the apiKey to set
     */
    public void setApiKey(String apiKey)
    {
        this.apiKey = apiKey;
    }
    
    /**
     * @return the message
     */
    public Message getMessage()
    {
        return message;
    }
    
    /**
     * @param message
     *            the message to set
     */
    public void setMessage(Message message)
    {
        this.message = message;
    }
    
}
