/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.req;

import com.aspire.commons.entities.Provider;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import net.sf.oval.constraint.Assert;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

/**
 * @see AdapterRequest
 * @see XmlElement
 * 
 * @author rajkumar.mardubudi
 */
@XmlRootElement
public class ProviderAdapterRequest implements AdapterRequest
{
    
    @NotNull
    @NotEmpty
    @Assert(expr = "_value.apiKey.id != undefined && _value.providerType == 'TWILIO_SMS' "
            + "|| _value.providerType == 'CLICKTELL_SMS' || "
            + "_value.providerType == 'FCM' || "
            + "_value.providerType == 'EMAIL' && _value.providerRecord != undefined", lang = "javascript")
    private Provider provider;
    
    public Provider getProvider()
    {
        return provider;
    }
    
    public void setProvider(Provider provider)
    {
        this.provider = provider;
    }
    
}
