/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.req;

import com.aspire.commons.entities.Query;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

/**
 * @see AdapterRequest
 * @see XmlElement
 */
@XmlRootElement
public class QueryAdapterRequest implements AdapterRequest
{
    @NotNull
    @NotEmpty
	protected Query query;

    /**
     * @return the query
     */
    @XmlElement(type=Query.class)
	public Query getQuery() 
    {
		return query;
	}

    /**
     * @param query the query to set
     */
	public void setQuery(Query query) 
	{
		this.query = query;
	}
}
