/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.res;

import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.Status;
import com.aspire.commons.engine.adaptors.req.AdapterRequest;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @see AdapterRequest
 * @see XmlElement
 */
@XmlRootElement
public class AdapterResponseBase implements AdapterResponse
{
    protected Status status = Status.GENERIC_FAILURE;
    protected long time;
    protected String version;
    protected List<ComplexMessage> errors = new ArrayList<ComplexMessage>();
    
    /**
     * @return the errors
     */
    public List<ComplexMessage> getErrors()
    {
        return errors;
    }
    
    /**
     * @param errors
     *            the errors to set
     */
    public void setErrors(List<ComplexMessage> errors)
    {
        this.errors = errors;
    }
    
    /**
     * @return the status
     */
    public Status getStatus()
    {
        return status;
    }
    
    /**
     * @param status
     *            the status to set
     */
    public void setStatus(Status status)
    {
        this.status = status;
    }
    
    /**
     * @return the time
     */
    public long getTime()
    {
        return time;
    }
    
    /**
     * @param time
     *            the time to set
     */
    public void setTime(long time)
    {
        this.time = time;
    }
    
    /**
     * @return the version
     */
    public String getVersion()
    {
        return version;
    }
    
    /**
     * @param version
     *            the version to set
     */
    public void setVersion(String version)
    {
        this.version = version;
    }
    
}
