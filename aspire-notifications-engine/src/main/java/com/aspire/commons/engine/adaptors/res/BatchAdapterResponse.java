/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.res;

import com.aspire.commons.entities.Batch;

import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @see AdapterResponseBase
 * @see XmlElement
 */
@XmlRootElement
public class BatchAdapterResponse extends AdapterResponseBase
{
    private Batch batch;
    
    private ConcurrentHashMap<String, String> inFlight = new ConcurrentHashMap<String, String>();
    
    /**
     * @return the batch
     */
    @XmlElement(type = Batch.class)
    public Batch getBatch()
    {
        return batch;
    }
    
    /**
     * @param batch
     *            the batch to set
     */
    public void setBatch(Batch batch)
    {
        this.batch = batch;
    }

    /**
     * @return the inFlight
     */
    public ConcurrentHashMap<String, String> getInFlight()
    {
        return inFlight;
    }

    /**
     * @param inFlight the inFlight to set
     */
    public void setInFlight(ConcurrentHashMap<String, String> inFlight)
    {
        this.inFlight = inFlight;
    }
    
}
