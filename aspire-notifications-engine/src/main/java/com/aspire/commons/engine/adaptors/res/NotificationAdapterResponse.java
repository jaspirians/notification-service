/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.res;

import com.aspire.commons.engine.adaptors.req.AdapterRequest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @see AdapterRequest
 * @see XmlElement
 * 
 * @author rajkumar.mardubudi
 */
@XmlRootElement
public class NotificationAdapterResponse extends AdapterResponseBase
{
    private String messageId;

    /**
     * @return the messageId
     */
    public String getMessageId()
    {
        return messageId;
    }

    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

   

    
    
}
