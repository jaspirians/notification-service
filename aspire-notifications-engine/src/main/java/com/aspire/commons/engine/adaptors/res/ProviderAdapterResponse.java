/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.res;

import com.aspire.commons.engine.adaptors.req.AdapterRequest;
import com.aspire.commons.entities.Provider;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @see AdapterRequest
 * @see XmlElement
 * 
 * @author rajkumar.mardubudi
 */
@XmlRootElement
public class ProviderAdapterResponse extends AdapterResponseBase
{
    protected Provider provider;
    
    /**
     * @return the provider
     */
    public Provider getProvider()
    {
        return provider;
    }
    
    /**
     * @param provider
     *            the provider to set
     */
    public void setProvider(Provider provider)
    {
        this.provider = provider;
    }
    
}
