package com.aspire.commons.engine.adaptors.res;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @see AdapterResponse
 * @see XmlElement
 */
@XmlRootElement
public class QueryAdapterResponse extends AdapterResponseBase 
{

	protected List<Object[]> records;
	
	/**
     * @return the records
     */
    @XmlElement(type=Object[].class)
	public List<Object[]> getRecords() {
		return records;
	}

	/**
     * @param records the records to set
     */
	public void setRecords(List<Object[]> records) {
		this.records = records;
	}
}
