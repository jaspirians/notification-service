/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.dao;

import com.aspire.commons.entities.ApiKey;

/**
 * @author rajkumar.mardubudi
 */
public class ApiKeyDao extends AbstractDAO<ApiKey>
{
    
    public ApiKeyDao()
    {
        super(ApiKey.class);
    }
    
}
