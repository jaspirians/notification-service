/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.dao;

public class DaoFactory
{
    private static ApiKeyDao apiKeyDao = new ApiKeyDao();
    private static ProviderDao providerDao = new ProviderDao();
    private static EmailDao emailDao = new EmailDao();
    private static SmsDao smsDao = new SmsDao();
    private static BatchDao batchDao = new BatchDao();
    private static BatchInstanceDao batchInstanceDao = new BatchInstanceDao();
    
    
    public ApiKeyDao getApiKeyDao()
    {
        return apiKeyDao;
    }
    
    public ProviderDao getProviderDao()
    {
        return providerDao;
    }

    /**
     * @return the emailDao
     */
    public static EmailDao getEmailDao()
    {
        return emailDao;
    }

    /**
     * @return the smsDao
     */
    public static SmsDao getSmsDao()
    {
        return smsDao;
    }
    
    /**
     * @return the batchDao
     */
    public static BatchDao getBatchDao()
    {
        return batchDao;
    }
    
    /**
     * @return the batchDao
     */
    public static BatchInstanceDao getBatchInstanceDao()
    {
        return batchInstanceDao;
    }
    
    
    
}
