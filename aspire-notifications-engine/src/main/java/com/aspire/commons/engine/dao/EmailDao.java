/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.dao;

import com.aspire.commons.entities.Email;

public class EmailDao extends AbstractDAO<Email>
{
    
    public EmailDao()
    {
        super(Email.class);
    }
    
}
