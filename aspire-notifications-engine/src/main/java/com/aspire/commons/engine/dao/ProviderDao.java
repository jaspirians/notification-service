/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.dao;

import com.aspire.commons.entities.Provider;

public class ProviderDao extends AbstractDAO<Provider>
{
    
    public ProviderDao()
    {
        super(Provider.class);
    }
    
}
