/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.dao;

import com.aspire.commons.entities.Sms;

public class SmsDao extends AbstractDAO<Sms>
{
    
    public SmsDao()
    {
        super(Sms.class);
    }
    
}
