/**
 * 
 */
package com.aspire.commons.engine.exe;

import com.aspire.commons.engine.adaptors.core.ComplexException;
import com.aspire.commons.engine.adaptors.core.ComplexMessage;

/**
 * @see ComplexException
 * @see ComplexMessage
 */
public class ServiceException extends ComplexException
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * @param msg
     * @param ex
     */
    public ServiceException(ComplexMessage msg, Throwable ex)
    {
        super(msg, ex);
    }
    
    /**
     * @param msg
     */
    public ServiceException(ComplexMessage msg)
    {
        super(msg);
    }
    
}
