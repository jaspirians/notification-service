/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.services;

import com.aspire.batch.handlers.BatchHandler;
import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.adaptors.core.Status;
import com.aspire.commons.engine.dao.DaoFactory;
import com.aspire.commons.engine.exe.ServiceException;
import com.aspire.commons.entities.Batch;
import com.aspire.commons.entities.BatchInstance;
import com.aspire.commons.logs.RLogFactory;

import org.apache.commons.logging.Log;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.security.auth.login.LoginException;

/**
 * @author rajkumar.mardubudi
 */
public class BatchService extends Service
        implements JobFactory
{
    protected static Log log = RLogFactory.getLog(BatchService.class);
    protected static MessageBundle bundle = new MessageBundle(
            BatchService.class);
    
    protected String username;
    protected String password;
    
    protected static String JOB_ClASS_NAME_REF = "jobClassNameRef";
    
    
    protected String loginModule = "ServiceLoginModule";
    
    private Scheduler scheduler = null;
    
    
    protected static ConcurrentHashMap<String, String> inFlight = new ConcurrentHashMap<String, String>();
    
    
    public BatchService()
    {
        try
        {
        start();
        init();
        success();
        }
        catch(Exception e)
        {
            fail(e);
            throw new ServiceException(new ComplexMessage("E_Init", bundle, e));
        }
        finally
        {
            end();
        }
    }
    
    /**
     * Initialization method
     * 
     * @throws SchedulerException
     * @throws ParseException
     * @throws LoginException
     */
    public void init() throws SchedulerException, ParseException,
            LoginException
    {
        
        SchedulerFactory schedFact = new StdSchedulerFactory();
        scheduler = schedFact.getScheduler();
        scheduler.start();
        
        List<Batch> defs = DaoFactory.getBatchDao().findAll();
        
        for (Batch bdef : defs)
        {
            JobKey jobKey = new JobKey(bdef.getId(), bdef.getId());
            JobDetail jd = JobBuilder.newJob(_job.class).withIdentity(jobKey)
                    .build();
            
            jd.getJobDataMap().put(JOB_ClASS_NAME_REF, bdef.getJobClass());
            
            Trigger trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity(bdef.getJobClass(), bdef.getId())
                    .withSchedule(
                            CronScheduleBuilder.cronSchedule(bdef.getTrigger()))
                    .build();
            scheduler.setJobFactory(this);
            scheduler.scheduleJob(jd, trigger);
        }
        log.info("Scheduler Initialized");
    }
    
    
    /**
     * Open a job
     */
    public BatchInstance open(final Batch currentBatch, final Date fireTime,
            final Date nextFireTime)
    {
        
        BatchInstance batchInstance = new BatchInstance();
        batchInstance.setBatch(currentBatch);
        batchInstance.setNextTime(nextFireTime);
        batchInstance.setStartTime(fireTime);
        batchInstance.setStatusCode(Status.OK);
        batchInstance = DaoFactory.getBatchInstanceDao().save(batchInstance);
        return batchInstance;
    }
    
    private void close(BatchInstance instance, Status status, String err)
    {
        
        BatchInstance batchInstanceImpl = DaoFactory.getBatchInstanceDao().findById(instance.getId());
        
        batchInstanceImpl.setStatusCode(status);
        batchInstanceImpl.setStopTime(new Date());
        batchInstanceImpl.setErrorMessage(err);
        DaoFactory.getBatchInstanceDao().update(batchInstanceImpl);
    }
    
    /**
     * Define a batch job
     */
    public Batch defineBatch(Batch definition) throws ServiceException
    {
        try
        {           
            start();
            Batch batch = DaoFactory.getBatchDao().save(definition);
            
            JobKey jobKey = new JobKey(batch.getId(), batch.getId());
            JobDetail jd = JobBuilder.newJob(_job.class).withIdentity(jobKey)
                    .build();
            jd.getJobDataMap().put(JOB_ClASS_NAME_REF, batch.getJobClass());
            
            Trigger triggerr = TriggerBuilder
                    .newTrigger()
                    .withIdentity(batch.getJobClass(), batch.getId())
                    .withSchedule(
                            CronScheduleBuilder.cronSchedule(batch.getTrigger()))
                    .build();
            
            scheduler.scheduleJob(jd, triggerr);
            scheduler.setJobFactory(this);
            success();
            return batch;
        }
        catch (SchedulerException se)
        {
            fail(se);
            log.error(se);
            throw new ServiceException(new ComplexMessage("E_BatchDef", bundle, definition.getName(), definition.getDescription()), se);
        }
        finally
        {
            end();
        }
    }
    
    /**
     * Disables the specified batch job
     */
    public Batch disableBatch(Batch batch) throws ServiceException
    {
        try
        {
            start();
            JobKey jobKey = new JobKey(batch.getId(), batch.getId());
            scheduler.pauseJob(jobKey);
            batch = DaoFactory.getBatchDao().update(batch);
            success();
        }
        catch (SchedulerException se)
        {
            fail(se);
            log.error(se);
            throw new ServiceException(new ComplexMessage("E_DisableBatch", bundle, batch.getId()), se);
        }
        finally
        {
            end();
        }
        return batch;
    }
    
    /**
     * Returns Batch definition based on id
     */
    public Batch getBatchDefs(String id)
    {
        Batch batch = null;
        try
        {
        start();
        batch = DaoFactory.getBatchDao().findById(id);
        success();
        }
        catch (Exception e)
        {
            fail(e);
            log.error(e);
            throw new ServiceException(new ComplexMessage("E_BatchDefs", bundle, id), e);
        }
        finally
        {
            end();
        }
        return batch;
    }
    
    /**
     * Executes a Batch job
     */
    public BatchInstance execBatch(Batch batch)
    {
        BatchInstance instance = null;
        try
        {
            log.info("START " + batch.getId());
            
            Batch cdef = getBatchDefs(batch.getId());
            
            // Get the batch class and call it
            
            String jclass = cdef.getJobClass();
            BatchHandler handler = (BatchHandler) Class.forName(jclass).newInstance();
            
            instance = open(cdef, new Date(), new Date());
            
            log.info("START " + cdef.getId());
            if (instance == null)
            {
                log.info("FINISH " + cdef.getId());
            }
            Status ret = Status.OK;
            String err = null;
            try
            {
                inFlight.put(batch.getId(), batch.getId());
                try
                {
                    handler.execute(instance);
                }
                finally
                {
                    inFlight.remove(batch.getId());
                }
            }
            catch (Exception e)
            {
                ret = Status.GENERIC_FAILURE;
                log.error(e);
                throw new ServiceException(new ComplexMessage("E_ExecBatch", bundle, cdef.getId()), e);
            }
            finally
            {
                close(instance, ret, err);
            }
        }
        catch (Exception e)
        {
            log.error(e);
            throw new ServiceException(new ComplexMessage("E_ExecBatch", bundle, batch.getId()), e);
        }
        
        return instance;
    }
    
    /**
     * Enable a batch job
     */
    public Batch enableBatch(Batch batch) throws ServiceException
    {
       
        try
        {
            start();
            JobKey jobKey = new JobKey(batch.getId(), batch.getId());
            scheduler.resumeJob(jobKey);
            batch = DaoFactory.getBatchDao().update(batch);
            success();
        }
        catch (Exception e)
        {
            fail(e);
            throw new ServiceException(new ComplexMessage("E_EnableBatch", bundle, batch.getId()), e);
            
        }
        finally
        {
            end();
        }
        return batch;
        
    }
    
    /**
     * Returns details on the jobs that are currently in-flight
     */
    public ConcurrentHashMap<String, String> getInFlight()
    {
        return inFlight;
    }
    
    /**
     * Updates a batch job configuration
     */
    public Batch updateBatch(Batch batch) throws ServiceException
    {
        JobKey jobKey = null;
        Batch def = null;
        try
        {
            start();
            if (batch.getTrigger() != null)
            {
                
                def = DaoFactory.getBatchDao().update(batch);
                jobKey = new JobKey(batch.getId(), batch.getId());
                scheduler.deleteJob(jobKey);
                JobDetail jd = JobBuilder.newJob(_job.class)
                        .withIdentity(jobKey).build();
                jd.getJobDataMap().put(JOB_ClASS_NAME_REF, batch.getJobClass());
                Trigger trigger = TriggerBuilder
                        .newTrigger()
                        .withIdentity(def.getJobClass(), def.getId())
                        .withSchedule(
                                CronScheduleBuilder.cronSchedule(def
                                        .getTrigger())).build();
                
                scheduler.setJobFactory(this);
                scheduler.scheduleJob(jd, trigger);
            }
            scheduler.resumeJob(jobKey);
            success();
        }
        catch (Exception e)
        {
            fail(e);
            throw new ServiceException(new ComplexMessage("E_UpdateBatch", bundle, batch.getId()), e);
        }
        finally
        {
            end();
        }
        
        return def;
    }
    
    @Override
    public Job newJob(TriggerFiredBundle tfb, Scheduler scheduler)
            throws SchedulerException
    {
        log.info("Starting " + tfb.getJobDetail().getKey().getName() + " "
                + tfb.getTrigger().getJobKey().getName() + " "
                + ((CronTrigger) tfb.getTrigger()).getExpressionSummary());
        return new _job();
    }
    
    
    /**
     * JOB class
     */
    public class _job implements Job
    {
        public void _execute(JobExecutionContext context)
                throws ServiceException
        {
            try
            {
                String bdefId = context.getJobDetail().getKey().getName();
                
                log.info("START Batch Job with Id [" + bdefId + "]");
                
                if (log.isDebugEnabled())
                {
                    log.debug("Starting");
                }
                
                Batch currentBatch = DaoFactory.getBatchDao().findById(bdefId);
                
                if (currentBatch != null && !currentBatch.isActive())
                {
                    log.info("I_JobDisabled [" + bdefId + "]");
                    log.info("FINISH Batch Job with Id ["
                            + currentBatch.getId() + "]");
                    return;
                }
                
                if (log.isDebugEnabled())
                {
                    log.debug("Running "+currentBatch.getId());
                }
                
                // Get the batch class and call it
                String jobClassNameRef = (String) context.getJobDetail()
                        .getJobDataMap().get(JOB_ClASS_NAME_REF);
                BatchHandler handler = (BatchHandler) Class.forName(jobClassNameRef).newInstance();
                
                BatchInstance instance = open(currentBatch,
                        context.getFireTime(), context.getNextFireTime());
                
                if (log.isDebugEnabled())
                {
                    log.debug("Opened "+currentBatch.getId());
                }
                
                if (instance == null)
                {
                    log.info("FINISH Batch Job with Id ["
                            + currentBatch.getId() + "]");
                    return;
                }
                
                if (instance.getBatch() != null)
                {
                    log.info("Commencing Batch Job ["
                            + instance.getBatch().getDescription()
                            + "] with Id [" + bdefId + "] with class ["
                            + instance.getBatch().getJobClass() + "]");
                    
                }
                Status status = Status.OK;
                String err = null;
                try
                {
                    inFlight.put(bdefId, bdefId);
                    try
                    {
                        if (log.isDebugEnabled())
                        {
                            log.debug("Exec "+currentBatch.getId());
                        }
                        
                        handler.execute(instance);
                    }
                    finally
                    {
                        inFlight.remove(bdefId);
                    }
                    
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    status = Status.GENERIC_FAILURE;
                    log.error(e);
                    throw new ServiceException(new ComplexMessage("E_ExecBatch", bundle), e);
                }
                finally
                {
                    close(instance, status, err);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                log.error(e);
                throw new ServiceException(new ComplexMessage("E_ExecBatch", bundle), e);
            }
            
        }
        
        @Override
        public void execute(JobExecutionContext context)
                throws ServiceException
        {
            try
            {
                start();
                _execute(context);
                success();
            }
            catch(Exception e)
            {
                fail(e);
                throw new ServiceException(new ComplexMessage("E_ExecBatch", bundle), e);
            }
            finally
            {
                end();
            }
            
        }
        
    }
    
}