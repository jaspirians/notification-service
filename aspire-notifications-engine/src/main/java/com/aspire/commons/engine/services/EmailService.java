/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.services;

import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.exe.ServiceException;
import com.aspire.commons.entities.Email;
import com.aspire.commons.entities.Provider;
import com.aspire.commons.logs.RLogFactory;
import com.aspire.commons.utils.Queue;
import com.aspire.commons.utils.State;

import org.apache.commons.logging.Log;

import java.util.Date;

public class EmailService extends Service
{
    protected static final Log log = RLogFactory.getLog(EmailService.class);
    protected static final MessageBundle bundle = new MessageBundle(
            EmailService.class);
    
    public Email save(Email email)
    {
        Email email2;
        MQClientService mqClientService = ServiceFactory.getMQClientService();
        try
        {
            start();
            Provider provider = factory_.getProviderDao().findById(email.getProvider().getId());
            email.setProvider(provider);
            email.setStatus(State.S_AWAITING_SEND.toString());
            email.setCreated(new Date());
            email = factory_.getEmailDao().save(email);
            email2 = factory_.getEmailDao().load(email.getId());
            mqClientService.send(Queue.EMAIL_QUEUE, email2);
            success();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail(e);
            ComplexMessage m = new ComplexMessage("E_SendEmail", bundle,
                    e.getMessage());
            throw new ServiceException(m);
        }
        finally
        {
            end();
        }
        return email;
    }
    
}
