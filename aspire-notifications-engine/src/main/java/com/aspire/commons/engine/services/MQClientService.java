/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.services;

import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.exe.ServiceException;
import com.aspire.commons.entities.AbstractEntity;
import com.aspire.commons.logs.RLogFactory;
import com.aspire.commons.utils.MQueueUtil;
import com.aspire.commons.utils.Queue;
import com.rabbitmq.client.Channel;

import org.apache.commons.logging.Log;

import java.io.IOException;
import java.io.StringWriter;

import javax.xml.bind.JAXB;

/**
 * @author rajkumar.mardubudi
 */
public class MQClientService extends Service
{
    protected static Log log = RLogFactory.getLog(MQClientService.class);
    protected static MessageBundle bundle = new MessageBundle(
            MQClientService.class);
    
    public void send(final Queue qName, final AbstractEntity entity)
            throws ServiceException, IOException
    {
        StringWriter sw = new StringWriter();
        JAXB.marshal(entity, sw);
       Channel channel = MQueueUtil.getChannel();
       channel.basicPublish("", qName.toString(), null, sw.toString().getBytes("UTF-8"));
    }

    
}
