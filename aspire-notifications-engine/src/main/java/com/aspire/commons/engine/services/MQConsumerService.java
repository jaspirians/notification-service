package com.aspire.commons.engine.services;

import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.dao.DaoFactory;
import com.aspire.commons.engine.dao.EmailDao;
import com.aspire.commons.engine.dao.SmsDao;
import com.aspire.commons.engine.exe.ServiceException;
import com.aspire.commons.entities.Email;
import com.aspire.commons.entities.Sms;
import com.aspire.commons.logs.RLogFactory;
import com.aspire.commons.utils.MQueueUtil;
import com.aspire.commons.utils.ProviderType;
import com.aspire.commons.utils.Queue;
import com.aspire.commons.utils.State;
import com.aspire.notifications.mail.EmailMessage;
import com.aspire.notifications.mail.config.EmailProvider;
import com.aspire.notifications.mail.service.EmailServiceImpl;
import com.aspire.notifications.sms.clickatell.config.ClickaTellProvider;
import com.aspire.notifications.sms.clickatell.msg.ClickaTellSmsMessage;
import com.aspire.notifications.sms.clickatell.service.ClickaTellServiceImpl;
import com.aspire.notifications.sms.twilio.config.TwilioProvider;
import com.aspire.notifications.sms.twilio.msg.SmsMessage;
import com.aspire.notifications.sms.twilio.service.TwilioServiceImpl;
import com.aspires.notifications.core.NotificationService;
import com.aspires.notifications.core.NotificationType;
import com.aspires.notifications.core.NotificationsFactory;
import com.aspires.notifications.msg.DefaultMessage;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import org.apache.commons.logging.Log;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.xml.bind.JAXB;

public class MQConsumerService extends Service
{
    protected static Log log = RLogFactory.getLog(MQConsumerService.class);
    protected static MessageBundle bundle = new MessageBundle(
            MQConsumerService.class);
    
    public MQConsumerService()
    {
    	Properties props = MQueueUtil.getEngineProperties();
    	System.setProperty("java.net.useSystemProxies", "true");
        System.setProperty("https.proxyHost", props.getProperty("proxyHost"));
        System.setProperty("https.proxyPort", props.getProperty("proxyPort"));
        props = null;
        init();
    }
    
    public void init()
    {
        try
        {
            receiveEmail();
            receiveSms();
            receiveFcm();
            log.info("Consumers started");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    private void receiveEmail() throws IOException
    {
        Channel channel = MQueueUtil.getChannel();
        Consumer consumer = new DefaultConsumer(channel)
        {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                    AMQP.BasicProperties properties, byte[] body)
                    throws IOException
            {
                try
                {
                    start();
                    
                    String message = new String(body, "UTF-8");
                    Email email = (Email) JAXB.unmarshal(
                            new ByteArrayInputStream(message.getBytes()),
                            Email.class);
                    EmailDao emailDao = DaoFactory.getEmailDao();
                    Email email2 = emailDao.load(email.getId());
                    if (State.S_AWAITING_SEND.toString()
                            .equals(email2.getStatus()))
                    {
                        System.out.println(" [x] Received Email '");
                        sendEmail(email);
                        email2.setStatus(State.S_SENT.toString());
                        email2.setModified(new Date());
                        emailDao.update(email2);
                    }
                    success();
                }
                catch (Exception e)
                {
                    fail(e);
                    ComplexMessage m = new ComplexMessage("E_SendEmail", bundle,
                            e.getMessage());
                    throw new ServiceException(m);
                }
                finally
                {
                    end();
                }
            }
        };
        
        channel.basicConsume(Queue.EMAIL_QUEUE.toString(), true, consumer);
    }
    
    private void receiveSms() throws IOException
    {
        
        log.info("Reading message from [" + Queue.SMS_QUEUE.toString()
                + " queue]");
        Channel channel = MQueueUtil.getChannel();
        Consumer consumer = new DefaultConsumer(channel)
        {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                    AMQP.BasicProperties properties, byte[] body)
                    throws IOException
            {
                try
                {
                    start();
                    String message = new String(body, "UTF-8");
                    Sms sms = (Sms) JAXB.unmarshal(new ByteArrayInputStream(message.getBytes()), Sms.class);
                    SmsDao smsDao = DaoFactory.getSmsDao();
                    Sms sms2 = smsDao.findById(sms.getId());
                    if (State.S_AWAITING_SEND.toString()
                            .equals(sms2.getStatus()))
                    {
                        System.out.println(" [x] Received SMS '");
                        String msgId = sendSms(sms);
                        System.out.println(msgId);
                        sms2.setStatus(State.S_SENT.toString());
                        sms2.setMsgId(msgId);
                        sms2.setModified(new Date());
                        smsDao.update(sms2);
                    }
                    
                    System.out.println(" [x] Received Sms '");
                    success();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    fail(e);
                    ComplexMessage m = new ComplexMessage("E_SendSms", bundle, e.getMessage());
                    throw new ServiceException(m);
                }
                finally
                {
                    end();
                }
            }
        };
        
        channel.basicConsume(Queue.SMS_QUEUE.toString(), true, consumer);
    }
    
    private void receiveFcm() throws IOException
    {
        log.info("Reading message from [" + Queue.FCM_QUEUE.toString()
                + " queue]");
        
        Channel channel = MQueueUtil.getChannel();
        Consumer consumer = new DefaultConsumer(channel)
        {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                    AMQP.BasicProperties properties, byte[] body)
                    throws IOException
            {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + message + "'");
            }
        };
        
        channel.basicConsume(Queue.FCM_QUEUE.toString(), true, consumer);
    }
    
    private void sendEmail(Email email)
    {
        try
        {
            System.out.println("sendEmail");
            EmailServiceImpl service = NotificationsFactory
                    .getService(EmailServiceImpl.class);
            service.buildConfiguration(email.getProvider().getProviderRecord(),
                    EmailProvider.class);
            System.out.println(service.getProvider());
            
            EmailMessage message = new EmailMessage(
                    service.createMimeMessage());
            message.setSubject(email.getSubject());
            message.setTo(email.getReceiver());
            message.setFrom(email.getSender());
            message.setText(email.getMessage(), true);
            service.send(message, NotificationType.String);
            System.out.println("sendEmail sent");
        }
        catch (MessagingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    
    private String sendSms(Sms sms)
    {
        if (ProviderType.CLICKTELL_SMS.toString()
                .equals(sms.getProvider().getProviderType()))
        {
            System.out.println(ProviderType.CLICKTELL_SMS +"Sending");
            NotificationService service = NotificationsFactory
                    .getService(ClickaTellServiceImpl.class);
            service.buildConfiguration(sms.getProvider().getProviderRecord(),
                    ClickaTellProvider.class);
            ClickaTellSmsMessage message = new ClickaTellSmsMessage();
            message.setTo(sms.getReceiver());
            message.setMessage(new DefaultMessage(sms.getMessage()));
            return service.send(message, NotificationType.String);
        }
        else if (ProviderType.TWILIO_SMS.toString()
                .equals(sms.getProvider().getProviderType()))
        {
            System.out.println(ProviderType.TWILIO_SMS +" Sending");
            NotificationService service = NotificationsFactory
                    .getService(TwilioServiceImpl.class);
            service.buildConfiguration(sms.getProvider().getProviderRecord(),
                    TwilioProvider.class);
            SmsMessage message = new SmsMessage();
            message.setTo(sms.getReceiver());
            message.setMessage(new DefaultMessage(sms.getMessage()));
            return service.send(message, NotificationType.String);
        }
        return null;
    }
}
