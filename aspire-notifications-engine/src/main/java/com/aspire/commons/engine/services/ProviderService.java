/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.services;

import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.exe.ServiceException;
import com.aspire.commons.entities.ApiKey;
import com.aspire.commons.entities.Provider;
import com.aspire.commons.logs.RLogFactory;

import org.apache.commons.logging.Log;

import java.util.Date;

public class ProviderService extends Service
{
    protected static final Log log = RLogFactory.getLog(ProviderService.class);
    protected static final MessageBundle bundle = new MessageBundle(
            ProviderService.class);
    
    public Provider registerProvider(Provider provider)
    {
        try
        {
            start();
            ApiKey apiKey = factory_.getApiKeyDao()
                    .findById(provider.getApiKey().getId());
            provider.setApiKey(apiKey);
            provider.setCreated(new Date());
            provider = factory_.getProviderDao().save(provider);
            success();
        }
        catch (ServiceException e)
        {
            fail(e);
            ComplexMessage m = new ComplexMessage("E_registerProvider", bundle,
                    e.getMessage());
            throw new ServiceException(m);
        }
        finally
        {
            end();
        }
        return provider;
    }
    
    public Provider getProviderById(String id)
    {
        
        Provider provider = null;
        
        try
        {
            start();
            provider = factory_.getProviderDao().findById(id);
            success();
        }
        catch (Exception e)
        {
            fail(e);
            ComplexMessage m = new ComplexMessage("E_ProviderGet", bundle,
                    e.getMessage());
            throw new ServiceException(m);
        }
        finally
        {
            end();
        }
        
        return provider;
        
    }
    
    public Provider update(Provider provider)
    {
        try
        {
            start();
            ApiKey apiKey = factory_.getApiKeyDao()
                    .findById(provider.getApiKey().getId());
            provider.setApiKey(apiKey);
            provider.setModified(new Date());
            provider = factory_.getProviderDao().update(provider);
            success();
        }
        catch (Exception e)
        {
            fail(e);
            ComplexMessage m = new ComplexMessage("E_providerUpdate", bundle,
                    e.getMessage());
            throw new ServiceException(m);
        }
        finally
        {
            end();
        }
        
        return provider;
    }
    
    public Provider remove(Provider provider)
    {
        try
        {
            start();
            factory_.getProviderDao().makeTransient(provider);
            success();
        }
        catch (Exception e)
        {
            fail(e);
            ComplexMessage m = new ComplexMessage("E_providerDelete", bundle,
                    e.getMessage());
            throw new ServiceException(m);
        }
        finally
        {
            end();
        }
        
        return provider;
    }
    
}
