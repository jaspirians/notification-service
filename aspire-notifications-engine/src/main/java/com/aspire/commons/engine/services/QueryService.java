/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.services;

import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.exe.ServiceException;
import com.aspire.commons.entities.Query;
import com.aspire.commons.logs.RLogFactory;
import com.aspire.commons.utils.HibernateUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.hibernate.SQLQuery;
import org.hibernate.jdbc.Work;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @see QueryService
 * @author rajkumar.mardubudi
 */
public class QueryService extends Service {

	protected static Log log = RLogFactory.getLog(QueryService.class);
	protected static MessageBundle bundle = new MessageBundle(QueryService.class);

	public QueryService()
	{
	    try
        {
            init();
        }
        catch (SQLException e)
        {
            ComplexMessage msg = new ComplexMessage("E_NoViewNameIsAvailabe", bundle, e.getMessage());
            log.error(msg);
            throw new ServiceException(msg);
        }
	}

	private static Set<String> viewNames = new HashSet<String>();

	public void init() throws SQLException 
	{
	    
	    try
	    {
	    start();
		HibernateUtil.getSessionFactory().getCurrentSession().doWork(new Work() 
		{
			@Override
			public void execute(Connection connection) throws SQLException 
			{
				readAllPublicViews(connection);
			}
		});
		success();
	    }catch(ServiceException exception)
	    {
	        fail(exception);
            ComplexMessage m = new ComplexMessage("E_readAllPublicViews", bundle,
                    exception.getMessage());
            throw new ServiceException(m);
	    }
		
		
	}

	/**
	 * @see com.mt.commons.engine.services.QueryService#query(Query)
	 */
	public List<Object[]> query(Query query) throws ServiceException
	{
		List<Object[]> result = null;
		if (query != null && StringUtils.isNotEmpty(query.getView())) 
		{
			if (viewNames.contains(query.getView())) 
			{
				try 
				{
				    start();
					ComplexMessage queryToBeExecute = new ComplexMessage(
							"Q_retriveRecordsFromView", bundle, query.getView()
									.toLowerCase(), query.getCondition());
					
					SQLQuery sqlQuery = HibernateUtil.getSessionFactory().getCurrentSession()
							.createSQLQuery(queryToBeExecute.getMessage());
					sqlQuery.setFirstResult(query.getStartPage());
					sqlQuery.setMaxResults(query.getPageSize());
					result = sqlQuery.list();
					success();
				}
				catch (Exception e) 
				{
				    fail(e);
				    e.printStackTrace();
					ComplexMessage msg = new ComplexMessage("E_InvalidSQLQuery", bundle);
					log.error(msg);
					throw new ServiceException(msg);
				}
				finally
				{
				    end();
				}

			} else 
			{
				ComplexMessage msg = new ComplexMessage("E_NoViewNameIsAvailabe", bundle, query.getView());
				log.error(msg);
				throw new ServiceException(msg);
			}
		}

		return result;
	}

	/**
	 * This method resposible to read all public views from db
	 * @param connection
	 * @throws SQLException
	 */
	private void readAllPublicViews(Connection connection) throws SQLException 
	{
	    DatabaseMetaData mdata = connection.getMetaData();
	    String [] types = {"VIEW"};
	    ResultSet rs = mdata.getTables(null, null, "pub_%", types);
	    rs.beforeFirst();
	    while(rs.next())
	    { 
	       viewNames.add((rs.getString("TABLE_NAME").substring(4)));
	    }
	    if(rs != null)
        {
            rs.close();
        }
	}
}
