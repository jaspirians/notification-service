/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.services;

public class ServiceFactory
{
    
    private static ProviderService providerService = new ProviderService();
    private static QueryService queryService = new QueryService();
    private static EmailService emailService = new EmailService();
    private static SmsService smsService = new SmsService();
    private static MQClientService mQClientService = new MQClientService();
    private static MQConsumerService mqConsumerService = new MQConsumerService();
    private static BatchService batchService = new BatchService();
    
    /**
     * @return the providerService
     */
    public static ProviderService getProviderService()
    {
        return providerService;
    }

    /**
     * @return the queryService
     */
    public static QueryService getQueryService()
    {
        return queryService;
    }

    /**
     * @return the emailService
     */
    public static EmailService getEmailService()
    {
        return emailService;
    }

    /**
     * @return the smsService
     */
    public static SmsService getSmsService()
    {
        return smsService;
    }

    public static MQClientService getMQClientService()
    {
        return mQClientService;
    }

    public static MQConsumerService getMqConsumerService()
    {
        return mqConsumerService;
    }
    
    public static BatchService getBatchService()
    {
        return batchService;
    }
    
}
