/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.services;

import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.exe.ServiceException;
import com.aspire.commons.entities.Provider;
import com.aspire.commons.entities.Sms;
import com.aspire.commons.logs.RLogFactory;
import com.aspire.commons.utils.Queue;
import com.aspire.commons.utils.State;

import org.apache.commons.logging.Log;

import java.util.Date;

public class SmsService extends Service
{
    protected static final Log log = RLogFactory.getLog(SmsService.class);
    protected static final MessageBundle bundle = new MessageBundle(
            SmsService.class);
    
    public Sms save(Sms sms)
    {
        MQClientService mqClientService = ServiceFactory.getMQClientService();
        try
        {
            start();
            Provider provider = factory_.getProviderDao().findById(sms.getProvider().getId());
            sms.setProvider(provider);
            sms.setStatus(State.S_AWAITING_SEND.toString());
            sms.setCreated(new Date());
            sms = factory_.getSmsDao().save(sms);
            mqClientService.send(Queue.SMS_QUEUE, sms);
            success();
        }
        catch (Exception e)
        {
            fail(e);
            ComplexMessage m = new ComplexMessage("E_SendSms", bundle,
                    e.getMessage());
            throw new ServiceException(m);
        }
        finally
        {
            end();
        }
        return sms;
    }
    
}
