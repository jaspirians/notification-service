/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.entities;

import java.util.Date;

public class ApiKey extends AbstractEntity
{
    
    private String appName;
    private String email;
    private String mobile;
    private String address;
    private String domain;
    private Integer quota;
    private Date created;
    private Date modified;
    private String createdBy;
    private String modifiedBy;
    
    /**
     * @return the appName
     */
    public String getAppName()
    {
        return appName;
    }
    
    /**
     * @param appName
     *            the appName to set
     */
    public void setAppName(String appName)
    {
        this.appName = appName;
    }
    
    /**
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }
    
    /**
     * @param email
     *            the email to set
     */
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    /**
     * @return the mobile
     */
    public String getMobile()
    {
        return mobile;
    }
    
    /**
     * @param mobile
     *            the mobile to set
     */
    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }
    
    /**
     * @return the address
     */
    public String getAddress()
    {
        return address;
    }
    
    /**
     * @param address
     *            the address to set
     */
    public void setAddress(String address)
    {
        this.address = address;
    }
    
    /**
     * @return the domain
     */
    public String getDomain()
    {
        return domain;
    }
    
    /**
     * @param domain
     *            the domain to set
     */
    public void setDomain(String domain)
    {
        this.domain = domain;
    }
    
    /**
     * @return the quota
     */
    public Integer getQuota()
    {
        return quota;
    }
    
    /**
     * @param quota
     *            the quota to set
     */
    public void setQuota(Integer quota)
    {
        this.quota = quota;
    }
    
    /**
     * @return the created
     */
    public Date getCreated()
    {
        return created;
    }
    
    /**
     * @param created
     *            the created to set
     */
    public void setCreated(Date created)
    {
        this.created = created;
    }
    
    /**
     * @return the modified
     */
    public Date getModified()
    {
        return modified;
    }
    
    /**
     * @param modified
     *            the modified to set
     */
    public void setModified(Date modified)
    {
        this.modified = modified;
    }
    
    /**
     * @return the createdBy
     */
    public String getCreatedBy()
    {
        return createdBy;
    }
    
    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * @return the modifiedBy
     */
    public String getModifiedBy()
    {
        return modifiedBy;
    }
    
    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }
    
}
