/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.entities;

import java.util.Date;

public class Provider extends AbstractEntity
{
    
    private ApiKey apiKey;
    private String providerType;
    private String providerRecord;
    private Date created;
    private Date modified;
    
    /**
     * @return the apiKey
     */
    public ApiKey getApiKey()
    {
        return apiKey;
    }
    
    /**
     * @param apiKey
     *            the apiKey to set
     */
    public void setApiKey(ApiKey apiKey)
    {
        this.apiKey = apiKey;
    }
    
    /**
     * @return the providerType
     */
    public String getProviderType()
    {
        return providerType;
    }
    
    /**
     * @param providerType
     *            the providerType to set
     */
    public void setProviderType(String providerType)
    {
        this.providerType = providerType;
    }
    
    /**
     * @return the providerRecord
     */
    public String getProviderRecord()
    {
        return providerRecord;
    }
    
    /**
     * @param providerRecord
     *            the providerRecord to set
     */
    public void setProviderRecord(String providerRecord)
    {
        this.providerRecord = providerRecord;
    }
    
    /**
     * @return the created
     */
    public Date getCreated()
    {
        return created;
    }
    
    /**
     * @param created
     *            the created to set
     */
    public void setCreated(Date created)
    {
        this.created = created;
    }
    
    /**
     * @return the modified
     */
    public Date getModified()
    {
        return modified;
    }
    
    /**
     * @param modified
     *            the modified to set
     */
    public void setModified(Date modified)
    {
        this.modified = modified;
    }
    
}
