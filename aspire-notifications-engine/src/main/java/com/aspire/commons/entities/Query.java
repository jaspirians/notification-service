/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.entities;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rajkumar.mardubudi
 * @see Query This class contains the query information like view name,
 *      condition, start page, page size
 */
@XmlRootElement
public class Query
{
    
    /**
     * Name of the view that exists in DB
     */
    private String view;
    
    /**
     * Condition is to retrieve records
     */
    private String condition;
    
    /**
     * Set the first row to retrieve.
     */
    private int startPage;
    
    /**
     * Set the maximum number of rows to retrieve
     */
    private int pageSize;
    
    public String getView()
    {
        return view;
    }
    
    public void setView(String view)
    {
        this.view = view;
    }
    
    public String getCondition()
    {
        return condition;
    }
    
    public void setCondition(String condition)
    {
        this.condition = condition;
    }
    
    public int getStartPage()
    {
        return startPage;
    }
    
    public void setStartPage(int startPage)
    {
        this.startPage = startPage;
    }
    
    public int getPageSize()
    {
        return pageSize;
    }
    
    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }
    
}
