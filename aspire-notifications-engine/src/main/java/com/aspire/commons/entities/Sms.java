package com.aspire.commons.entities;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Sms extends AbstractEntity
{
    
    private Provider provider;
    private String message;
    private String sender;
    private String receiver;
    private String msgId;
    private String status;
    private Date created;
    private Date modified;
    
    /**
     * @return the provider
     */
    public Provider getProvider()
    {
        return provider;
    }
    /**
     * @param provider the provider to set
     */
    public void setProvider(Provider provider)
    {
        this.provider = provider;
    }
    /**
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }
    /**
     * @param message the message to set
     */
    public void setMessage(String message)
    {
        this.message = message;
    }
    /**
     * @return the sender
     */
    public String getSender()
    {
        return sender;
    }
    /**
     * @param sender the sender to set
     */
    public void setSender(String sender)
    {
        this.sender = sender;
    }
    
    /**
     * @return the receiver
     */
    public String getReceiver()
    {
        return receiver;
    }
    /**
     * @param receiver the receiver to set
     */
    public void setReceiver(String receiver)
    {
        this.receiver = receiver;
    }
    /**
     * @return the msgId
     */
    public String getMsgId()
    {
        return msgId;
    }
    /**
     * @param msgId the msgId to set
     */
    public void setMsgId(String msgId)
    {
        this.msgId = msgId;
    }
    /**
     * @return the status
     */
    public String getStatus()
    {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String status)
    {
        this.status = status;
    }
    /**
     * @return the created
     */
    public Date getCreated()
    {
        return created;
    }
    /**
     * @param created the created to set
     */
    public void setCreated(Date created)
    {
        this.created = created;
    }
    /**
     * @return the modified
     */
    public Date getModified()
    {
        return modified;
    }
    /**
     * @param modified the modified to set
     */
    public void setModified(Date modified)
    {
        this.modified = modified;
    }

	@Override
	public String toString() {
		return "Sms [provider=" + provider + ", message=" + message + ", sender=" + sender + ", receiver=" + receiver
				+ ", msgId=" + msgId + ", status=" + status + ", created=" + created + ", modified=" + modified + "]";
	}
    
}
