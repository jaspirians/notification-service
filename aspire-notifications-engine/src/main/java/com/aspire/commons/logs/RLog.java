/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.logs;

import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.MessageBundle;

import org.apache.commons.collections.Closure;
import org.apache.commons.logging.Log;

import java.lang.annotation.Annotation;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.HashSet;

/**
 */
public class RLog implements Log
{
    protected Log log = null;
    protected MessageBundle bundle = null;
    
    /**
     * @param bundle
     */
    RLog(Log log, MessageBundle bundle)
    {
        this.bundle = bundle;
        this.log = log;
    }
    
    protected Object make_message(Object m)
    {
        return make_message(m, null);
    }
    
    protected Object make_message(Object m, Throwable e)
    {
        return m;
    }
    
    /**
     * @param arg0
     * @param arg1
     * @see org.apache.commons.logging.Log#debug(java.lang.Object,
     *      java.lang.Throwable)
     */
    public void debug(Object m, Throwable e)
    {
        if (!log.isDebugEnabled())
        {
            return;
        }
        log.debug(make_message(m), e);
    }
    
    /**
     * @param arg0
     * @see org.apache.commons.logging.Log#debug(java.lang.Object)
     */
    public void debug(Object m)
    {
        if (!log.isDebugEnabled())
        {
            return;
        }
        log.debug(make_message(m));
    }
    
    /**
     * @param arg0
     * @param arg1
     * @see org.apache.commons.logging.Log#error(java.lang.Object,
     *      java.lang.Throwable)
     */
    public void error(Object m, Throwable e)
    {
        if (!log.isErrorEnabled())
        {
            return;
        }
        log.error(make_message(m), e);
    }
    
    /**
     * @param arg0
     * @see org.apache.commons.logging.Log#error(java.lang.Object)
     */
    public void error(Object m)
    {
        if (!log.isErrorEnabled())
        {
            return;
        }
        log.error(make_message(m));
    }
    
    /**
     * @param arg0
     * @param arg1
     * @see org.apache.commons.logging.Log#fatal(java.lang.Object,
     *      java.lang.Throwable)
     */
    public void fatal(Object m, Throwable e)
    {
        if (!log.isFatalEnabled())
        {
            return;
        }
        log.fatal(make_message(m), e);
    }
    
    /**
     * @param arg0
     * @see org.apache.commons.logging.Log#fatal(java.lang.Object)
     */
    public void fatal(Object m)
    {
        if (!log.isFatalEnabled())
        {
            return;
        }
        log.fatal(make_message(m));
    }
    
    /**
     * @param arg0
     * @param arg1
     * @see org.apache.commons.logging.Log#info(java.lang.Object,
     *      java.lang.Throwable)
     */
    public void info(Object m, Throwable e)
    {
        if (!log.isInfoEnabled())
        {
            return;
        }
        log.info(make_message(m), e);
    }
    
    /**
     * @param arg0
     * @see org.apache.commons.logging.Log#info(java.lang.Object)
     */
    public void info(Object m)
    {
        if (!log.isInfoEnabled())
        {
            return;
        }
        log.info(make_message(m));
    }
    
    /**
     * @return
     * @see org.apache.commons.logging.Log#isDebugEnabled()
     */
    public boolean isDebugEnabled()
    {
        return log.isDebugEnabled();
    }
    
    /**
     * @return
     * @see org.apache.commons.logging.Log#isErrorEnabled()
     */
    public boolean isErrorEnabled()
    {
        return log.isErrorEnabled();
    }
    
    /**
     * @return
     * @see org.apache.commons.logging.Log#isFatalEnabled()
     */
    public boolean isFatalEnabled()
    {
        return log.isFatalEnabled();
    }
    
    /**
     * @return
     * @see org.apache.commons.logging.Log#isInfoEnabled()
     */
    public boolean isInfoEnabled()
    {
        return log.isInfoEnabled();
    }
    
    /**
     * @return
     * @see org.apache.commons.logging.Log#isTraceEnabled()
     */
    public boolean isTraceEnabled()
    {
        return log.isTraceEnabled();
    }
    
    /**
     * @return
     * @see org.apache.commons.logging.Log#isWarnEnabled()
     */
    public boolean isWarnEnabled()
    {
        return log.isWarnEnabled();
    }
    
    /**
     * @param arg0
     * @param arg1
     * @see org.apache.commons.logging.Log#trace(java.lang.Object,
     *      java.lang.Throwable)
     */
    public void trace(Object m, Throwable e)
    {
        if (!log.isTraceEnabled())
        {
            return;
        }
        log.trace(make_message(m), e);
    }
    
    /**
     * @param arg0
     * @see org.apache.commons.logging.Log#trace(java.lang.Object)
     */
    public void trace(Object m)
    {
        if (!log.isTraceEnabled())
        {
            return;
        }
        log.trace(make_message(m));
    }
    
    /**
     * @param arg0
     * @param arg1
     * @see org.apache.commons.logging.Log#warn(java.lang.Object,
     *      java.lang.Throwable)
     */
    public void warn(Object m, Throwable e)
    {
        if (!log.isWarnEnabled())
        {
            return;
        }
        log.warn(make_message(m), e);
    }
    
    /**
     * @param arg0
     * @see org.apache.commons.logging.Log#warn(java.lang.Object)
     */
    public void warn(Object m)
    {
        if (!log.isWarnEnabled())
        {
            return;
        }
        log.warn(make_message(m));
    }
}
