/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.logging.Log;

import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.exe.ServiceException;
import com.aspire.commons.engine.services.ProviderService;
import com.aspire.commons.logs.RLogFactory;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author rajkumar.mardubudi
 */
public final class MQueueUtil
{
    protected static final Log log = RLogFactory.getLog(ProviderService.class);
    protected static final MessageBundle bundle = new MessageBundle(
            ProviderService.class);
    private MQueueUtil()
    {
        
    }
    // MQ properties names are below
    private static final String FILE_NAME = "Engine.properties";
    private static final String MQ_USER_NAME = "mqUserName";
    private static final String MQ_PASSWORD = "mqPassword";
    private static final String MQ_HOST = "mqHost";

    private static final Properties ENGINE_PROPERTIES = getRabbitMQProperties();    
    private static Channel channel = getMQChannel();
    
    private static Channel getMQChannel()
    {
        log.info("Connection is going to create");
        Channel channel = null;
        try
        {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUsername(ENGINE_PROPERTIES.getProperty(MQ_USER_NAME));
            factory.setPassword(ENGINE_PROPERTIES.getProperty(MQ_PASSWORD));
            factory.setHost(ENGINE_PROPERTIES.getProperty(MQ_HOST));
            Connection connection = factory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(Queue.EMAIL_QUEUE.toString(), false, false, false, null);
            channel.queueDeclare(Queue.SMS_QUEUE.toString(), false, false, false, null);
            channel.queueDeclare(Queue.FCM_QUEUE.toString(), false, false, false, null);
            log.info("Connection is created");
        }
        catch (Exception  e)
        {
            ComplexMessage m = new ComplexMessage("E_MQConnection", bundle, e.getMessage());
            throw new ServiceException(m);
        }
        
        return channel;
    }
    
    private static Properties getRabbitMQProperties() {
    	log.info("Reading MQ properties");
    	Properties prop = new Properties();
    	InputStream input = null;
    	try {    		
    		input = MQueueUtil.class.getClassLoader().getResourceAsStream(FILE_NAME);
			if (input == null) {
    			throw new IOException("Sorry, unable to find " + FILE_NAME);    		    
    		}
    		prop.load(input);
    		log.info("MQ properties loaded");
    	} catch (IOException e) {
    		ComplexMessage m = new ComplexMessage("E_MQConnectionProperties", bundle, e.getMessage());
            throw new ServiceException(m);
    	}
    	return prop;
	}

	public static Channel getChannel()
    {
        return channel;
    }

	public static Properties getEngineProperties() {
		return ENGINE_PROPERTIES;
	}
}
