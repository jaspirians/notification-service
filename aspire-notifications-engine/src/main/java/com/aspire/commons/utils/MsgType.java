/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.utils;

public enum MsgType
{
    EMAIL, SMS, FCM
}
