/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.utils;

import com.aspire.commons.crypt.StrongIDGenerator;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

/**
 * @see IdentifierGenerator
 * @see StrongIDGenerator
 */
public class PrimaryKeyGenerator implements IdentifierGenerator
{
    protected static StrongIDGenerator generator = new StrongIDGenerator();
    protected int strength = 1;
    
    public PrimaryKeyGenerator()
    {
        try
        {
            generator.init();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     * @return the strength
     */
    public int getStrength()
    {
        return strength;
    }
    
    /**
     * @param strength
     *            the strength to set
     */
    public void setStrength(int strength)
    {
        this.strength = strength;
    }
    
    /**
     * @return the generator
     */
    public StrongIDGenerator getGenerator()
    {
        return PrimaryKeyGenerator.generator;
    }
    
    /**
     * @param generator
     *            the generator to set
     */
    public void setGenerator(StrongIDGenerator generator)
    {
        PrimaryKeyGenerator.generator = generator;
    }
    
    /**
     * @see org.hibernate.id.IdentifierGenerator#generate(org.hibernate.engine.spi.SessionImplementor,
     *      java.lang.Object)
     */
    public Serializable generate(SessionImplementor session, Object object)
            throws HibernateException
    {
        return generator.generateId(this.strength);
    }
}
