/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.utils;

public enum ProviderType
{
    EMAIL, TWILIO_SMS, CLICKTELL_SMS, FCM
}
