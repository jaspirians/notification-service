package com.aspire.commons.utils;

public enum Queue
{
    EMAIL_QUEUE, SMS_QUEUE, FCM_QUEUE
}
