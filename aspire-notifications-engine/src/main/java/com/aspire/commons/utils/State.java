/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.utils;

/**
 * The set of states the is supported by current business process.
 */
public enum State
{
	S_AWAITING_SEND, S_ERROR, S_CONFIRM, S_VOID, S_SENT;
}
