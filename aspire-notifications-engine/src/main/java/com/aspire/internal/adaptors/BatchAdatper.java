/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.internal.adaptors;

import com.aspire.commons.engine.adaptors.AdapterBase;
import com.aspire.commons.engine.adaptors.core.Status;
import com.aspire.commons.engine.adaptors.req.BatchAdapterRequest;
import com.aspire.commons.engine.adaptors.res.BatchAdapterResponse;
import com.aspire.commons.engine.adaptors.res.QueryAdapterResponse;
import com.aspire.commons.engine.services.BatchService;
import com.aspire.commons.engine.services.QueryService;
import com.aspire.commons.engine.services.ServiceFactory;
import com.aspire.commons.entities.Batch;
import com.aspire.commons.entities.BatchInstance;
import com.aspire.commons.entities.Query;

import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/internal", description= "Batch Adapter" )
@Path("/internal")
public class BatchAdatper extends AdapterBase
{
    
    
    /**
     * Query interface used for Query of BatchDefs table
     * 
     * @return
     */
    @ApiOperation(value = "/batch/list", notes = "Get Batch Details")
    @POST
    @Path("/batch/list")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public QueryAdapterResponse getInfo()
    {
        long tm = System.currentTimeMillis();
        Query qw = new Query();
        qw.setView("batch");
        qw.setCondition("active = 1 || active = 0");
        qw.setStartPage(0);
        qw.setPageSize(100);
        
        QueryAdapterResponse response = new QueryAdapterResponse();
        
        QueryService service = ServiceFactory.getQueryService();
        
        List<Object[]> records = service.query(qw);
        response.setRecords(records);
        response.setStatus(Status.OK);
        response.setTime(System.currentTimeMillis() - tm);
        return response;
    }
    
    /**
     * Define a batch job
     * 
     * @param req
     * @return
     * @throws SchedulerException
     * @throws ParseException
     */
    @ApiOperation(value = "/batch/define", notes = "Get batch definition")
    @POST
    @Path("/batch/define")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public BatchAdapterResponse defineBatch(BatchAdapterRequest req)
            throws SchedulerException, ParseException
    {
        long tm = System.currentTimeMillis();
        BatchService batchService = ServiceFactory.getBatchService();
        
        BatchAdapterResponse response = new BatchAdapterResponse();
        Batch def = batchService.defineBatch(req.getBatch());
        
        response.setBatch(def);
        response.setStatus(Status.OK);
        response.setTime(System.currentTimeMillis() - tm);
        return response;
    }
    
    /**
     * Disables the specified batch job
     * 
     * @param req
     * @return
     * @throws SchedulerException
     * @throws ParseException
     */
    @ApiOperation(value = "/batch/disable", notes = "Disable batch")
    @POST
    @Path("/batch/disable")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public BatchAdapterResponse disableBatch(BatchAdapterRequest req)
            throws SchedulerException, ParseException
    {
        long tm = System.currentTimeMillis();
        BatchService batchService = ServiceFactory.getBatchService();
        
        BatchAdapterResponse response = new BatchAdapterResponse();
        Batch def = batchService.disableBatch(req.getBatch());
        response.setBatch(def);
        if (def != null)
        {
            log.info("Disabled Batch [" + req.getBatch().getId() + "], ["
                    + def.getDescription() + "]");
        }
        response.setStatus(Status.OK);
        response.setTime(System.currentTimeMillis() - tm);
        return response;
    }
    
    /**
     * Executes a Batch job
     * 
     * @param req
     * @return
     * @throws SchedulerException
     * @throws ParseException
     */
    @ApiOperation(value = "/batch/execute", notes = "Execute on demand")
    @POST
    @Path("/batch/execute")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public BatchAdapterResponse execBatch(BatchAdapterRequest req)
            throws SchedulerException, ParseException
    {
        long tm = System.currentTimeMillis();
        BatchAdapterResponse response = new BatchAdapterResponse();
        try
        {
            log.info("START " + req.getBatch().getId());
            
            BatchService batchService = ServiceFactory.getBatchService();
            BatchInstance instance = batchService.execBatch(req.getBatch());
            
        }
        catch (Exception e)
        {
            log.error(e);
            e.printStackTrace();
            throw new JobExecutionException(e);
        }
        response.setStatus(Status.OK);
        response.setTime(System.currentTimeMillis() - tm);
        return response;
    }
    
    
    /**
     * Enable a batch job
     * @param req
     * @return
     * @throws SchedulerException
     * @throws ParseException
     */
    @ApiOperation(value = "/batch/enable", notes = "Enable batch")
    @POST 
    @Path("/batch/enable") 
    @Consumes({MediaType.APPLICATION_JSON}) 
    @Produces({MediaType.APPLICATION_JSON}) 
    public BatchAdapterResponse enableBatch(BatchAdapterRequest req) throws SchedulerException, ParseException
    {
        long tm = System.currentTimeMillis();
        BatchService batchService = ServiceFactory.getBatchService();
        BatchAdapterResponse response = new BatchAdapterResponse();
        Batch def = batchService.enableBatch(req.getBatch());
        response.setBatch(def);
        response.setStatus(Status.OK);
        if (def != null)
        {
            log.info("Enabled Batch [" + req.getBatch().getId() + "], ["
                    + def.getDescription() + "]");
        }
        response.setTime(System.currentTimeMillis() - tm);
        return response;
    }

    /**
     * Returns details on the jobs that are currently in-flight
     * @return
     * @throws SchedulerException
     * @throws ParseException
     */
    @ApiOperation(value = "/batch/inflight", notes = "In flight")
    @POST 
    @Path("/batch/inflight") 
    @Produces({MediaType.APPLICATION_JSON}) 
    public BatchAdapterResponse inFlight() throws SchedulerException, ParseException
    {
        long tm = System.currentTimeMillis();
        BatchService batchService = ServiceFactory.getBatchService();
        BatchAdapterResponse response = new BatchAdapterResponse();
        response.getInFlight().putAll(batchService.getInFlight());
        response.setStatus(Status.OK);
        response.setTime(System.currentTimeMillis() - tm);
        return response;
    }
    
    /**
     * Updates a batch job configuration
     * @param req
     * @return
     * @throws SchedulerException
     * @throws ParseException
     */
    @ApiOperation(value = "/batch/update", notes = "Update batch")
    @POST 
    @Path("/batch/update") 
    @Consumes({MediaType.APPLICATION_JSON}) 
    @Produces({MediaType.APPLICATION_JSON}) 
    public BatchAdapterResponse updateBatch(BatchAdapterRequest req) throws SchedulerException, ParseException
    {
        long tm = System.currentTimeMillis();
        BatchService batchService = ServiceFactory.getBatchService();
        BatchAdapterResponse response = new BatchAdapterResponse();
        Batch def = batchService.updateBatch(req.getBatch());
        response.setBatch(def);
        response.setStatus(Status.OK);
        response.setTime(System.currentTimeMillis() - tm);
        return response;
    }
}