package com.aspires.notifications.engine;

import com.aspire.commons.engine.adaptors.ProviderAdapter;
import com.aspire.commons.engine.adaptors.core.Status;
import com.aspire.commons.engine.adaptors.req.AdapterRequest;
import com.aspire.commons.engine.adaptors.req.ProviderAdapterRequest;
import com.aspire.commons.engine.adaptors.res.AdapterResponse;
import com.aspire.commons.engine.adaptors.res.ProviderAdapterResponse;
import com.aspire.commons.utils.jaxb.JAXBRegistry;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.reflections.Reflections;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Set;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlRootElement;

public abstract class AbstractTestAdapter extends JerseyTest
{
    
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        
    }
    
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception
    {
        
    }
    
    @Before
    public void setUp() throws Exception
    {
        super.setUp();
    }
    
    /**
     * @see org.glassfish.jersey.test.JerseyTest#tearDown()
     */
    @Override
    public void tearDown() throws Exception
    {
        super.tearDown();
    }
    
    /**
     * @throws java.lang.Exception
     * @Before public void setUp() throws Exception { Client c =
     *         ClientBuilder.newClient();
     * 
     *         this.setClient(c ); }
     * @After public void tearDown() throws Exception { }
     */
    
    @Override
    protected Application configure()
    {
        ResourceConfig res = new ResourceConfig(ProviderAdapter.class);
        res.packages("com.aspire");
        res.register(JAXBRegistry.class);
        res.register(ProviderAdapterResponse.class);
        res.register(ProviderAdapterRequest.class);
        return res;
    }
    
    public AdapterResponse testAdapter(final String restMethod,
            final String path, final Class<?> rClass, final Class<?> respClass,
            final ClassPathResource request) throws JAXBException
    {
        try
        {
            return testAdapter(restMethod, path, rClass, respClass,
                    request.getInputStream());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public AdapterResponse testAdapter(final String restMethod,
            final String path, final Class<?> rClass, final Class<?> respClass,
            InputStream request) throws JAXBException
    {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setAnnotationIntrospector(
                new JaxbAnnotationIntrospector(mapper.getTypeFactory()));
        AdapterRequest req = null;
        try
        {
            if (request != null)
            {
                req = (AdapterRequest) mapper.readValue(request, rClass);
            }
        }
        catch (com.fasterxml.jackson.core.JsonParseException e1)
        {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
        catch (com.fasterxml.jackson.databind.JsonMappingException e1)
        {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
        return testAdapter(restMethod, path, rClass, respClass, req);
    }
    
    /**
     * Generic function that allows for any adapter to be executed.
     * 
     * @param adapter
     *            name of the adapter
     * @param restMethod
     *            rest method to use
     * @param path
     *            path of the test
     * @param rClass
     *            request class
     * @param respClass
     *            response class
     * @param request
     *            the actual resource with json of the request.
     * @return
     * @throws JAXBException
     */
    public AdapterResponse testAdapter(final String restMethod,
            final String path, final Class<?> rClass, final Class<?> respClass,
            final AdapterRequest req) throws JAXBException
    {
        
        ObjectMapper mapper = new ObjectMapper();
        Reflections reflections = new Reflections("com.aspire");
        Set<Class<?>> cls = reflections
                .getTypesAnnotatedWith(XmlRootElement.class);
        final ArrayList<Class<?>> noNullList = new ArrayList<Class<?>>();
        noNullList.addAll(cls);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                false);
        mapper.setVisibility(PropertyAccessor.ALL,
                JsonAutoDetect.Visibility.ANY);
        mapper.setAnnotationIntrospector(
                new JaxbAnnotationIntrospector(mapper.getTypeFactory()));
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        mapper.registerSubtypes(noNullList.toArray(new Class<?>[0]));
        
        Entity<AdapterRequest> entity = null;
        if (req != null)
        {
            entity = Entity.entity(req, MediaType.APPLICATION_JSON);
        }
        
        Response resp = null;
        try
        {
            if (entity != null)
            {
                resp = (Response) MethodUtils.invokeExactMethod(
                        target(path).request(), restMethod, entity);
            }
            else
            {
                Object[] arg = new Object[0];
                resp = (Response) MethodUtils.invokeExactMethod(
                        target(path).request(), restMethod, arg);
            }
        }
        catch (NoSuchMethodException e1)
        {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
        catch (IllegalAccessException e1)
        {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
        catch (InvocationTargetException e1)
        {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
        finally
        {
            // Checkpoint work after each simulation
            // EngineContext ec = EngineContext.current();
            // ec.checkpointWork();
        }
        
        StringWriter sw = new StringWriter();
        try
        {
            IOUtils.copy((InputStream) resp.getEntity(), sw);
            System.out.println(sw.toString());
            return (AdapterResponse) mapper.readValue(sw.toString(), respClass);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        
    }
    
}
