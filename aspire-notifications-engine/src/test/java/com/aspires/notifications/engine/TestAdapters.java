package com.aspires.notifications.engine;

import com.aspire.commons.engine.adaptors.core.Status;
import com.aspire.commons.engine.adaptors.req.NotificationAdapterRequest;
import com.aspire.commons.engine.adaptors.req.ProviderAdapterRequest;
import com.aspire.commons.engine.adaptors.req.QueryAdapterRequest;
import com.aspire.commons.engine.adaptors.res.NotificationAdapterResponse;
import com.aspire.commons.engine.adaptors.res.ProviderAdapterResponse;
import com.aspire.commons.engine.adaptors.res.QueryAdapterResponse;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import javax.xml.bind.JAXBException;

public class TestAdapters extends AbstractTestAdapter
{
    
    @Test
    public void testProviderAdapter() throws JAXBException
    {
        //Create a provider
        ProviderAdapterResponse pm = (ProviderAdapterResponse) testAdapter(
                "post", "/services/provider/register",
                ProviderAdapterRequest.class, ProviderAdapterResponse.class,
                new ClassPathResource("test/provider4.json"));
        Assert.assertEquals(pm.getStatus(), Status.OK);
        
        /*
        testAdapter("delete", "/services/provider/asdfad/"+pm.getProvider().getId(),
                ProviderAdapterRequest.class,
                ProviderAdapterResponse.class, (ProviderAdapterRequest)null);
        */
    }
    
    @Test
    public void testQueryAdapter() throws JAXBException
    {
        QueryAdapterResponse pm = (QueryAdapterResponse) testAdapter(
                "post", "/services/query",
                QueryAdapterRequest.class, QueryAdapterResponse.class,
                new ClassPathResource("test/query.json"));
        Assert.assertEquals(pm.getStatus(), Status.OK);
        
        
    }
    
    @Test
    public void testEmailNotifcationAdapter() throws JAXBException
    {
        NotificationAdapterResponse pm = (NotificationAdapterResponse) testAdapter(
                "post", "/services/send",
                NotificationAdapterRequest.class, NotificationAdapterResponse.class,
                new ClassPathResource("test/sendEmail.json"));
        Assert.assertEquals(pm.getStatus(), Status.OK);
        
        
    }
    
    @Test
    public void testSmsNotifcationAdapter() throws JAXBException
    {
        NotificationAdapterResponse pm = (NotificationAdapterResponse) testAdapter(
                "post", "/services/send",
                NotificationAdapterRequest.class, NotificationAdapterResponse.class,
                new ClassPathResource("test/sendSms.json"));
        Assert.assertEquals(pm.getStatus(), Status.OK);
    }
    
}
