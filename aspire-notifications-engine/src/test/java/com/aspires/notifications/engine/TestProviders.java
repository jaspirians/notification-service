package com.aspires.notifications.engine;

import com.aspire.notifications.mail.EmailMessage;
import com.aspire.notifications.mail.service.EmailService;
import com.aspire.notifications.mail.service.EmailServiceImpl;
import com.aspire.notifications.push.fcm.msg.FcmMessage;
import com.aspire.notifications.push.fcm.service.FcmNotificationService;
import com.aspire.notifications.sms.clickatell.msg.ClickaTellSmsMessage;
import com.aspire.notifications.sms.clickatell.service.ClickaTellServiceImpl;
import com.aspire.notifications.sms.twilio.msg.SmsMessage;
import com.aspire.notifications.sms.twilio.service.TwilioServiceImpl;
import com.aspires.notifications.core.NotificationMessage;
import com.aspires.notifications.core.NotificationService;
import com.aspires.notifications.core.NotificationType;
import com.aspires.notifications.core.NotificationsFactory;
import com.aspires.notifications.echo.EchoNotificationService;
import com.aspires.notifications.msg.DefaultMessage;
import com.aspires.notifications.msg.TemplatedMessage;
import com.aspires.notifications.otp.OTP;

import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

public class TestProviders {

	@Test
	public void testEchoService() {
		NotificationService service = NotificationsFactory.getService(EchoNotificationService.class);
		service.send(null, NotificationType.String);
	}
	
	@Test
	public void testEmailService()throws Exception {
		
		EmailService service = NotificationsFactory.getService(EmailServiceImpl.class);
		service.buildConfiguration("test/emailConfig.xml");
		
		EmailMessage message = new EmailMessage(service.createMimeMessage());
		
		message.setFrom("karthick.murugan@aspiresys.com");
		message.setSubject("Heelooooo");
		message.setTo(new String [] {"karthick.murugan@aspiresys.com"});
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("url", "some url");
		
		//message.setVelocity("/templates/dummy.vm", map);
		
		message.setText("Hiiiiiiiiiiii");
		
		service.send(message, NotificationType.String);
		
	}
	
	@Test
	public void testTwiloSmsService()throws Exception {
		NotificationService service = NotificationsFactory.getService(TwilioServiceImpl.class);
		service.buildConfiguration("test/twilo-sms.xml");
		
		SmsMessage message = new SmsMessage();
		message.setTo("+91888888888");
		message.setMessage(new DefaultMessage("Hiiiiiiiiiiiii Abc"));
		service.send(message, NotificationType.String);
		
		
		/*
		SmsMessage message = new SmsMessage();
		message.setTo("+918056703314");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("url", "some url");
		map.put("otp", OTP.generate());
		TemplatedMessage templatedMessage = new TemplatedMessage("/templates/dummy.vm", map);
		message.setMessage(templatedMessage);
		service.send(message, NotificationType.String);
		*/
	}
	
	
	@Test
	public void testEmailServiceWithOtp() throws MessagingException {
		
		EmailService service = NotificationsFactory.getService(EmailServiceImpl.class);
		service.buildConfiguration("test/emailConfig.xml");
		
		EmailMessage message = new EmailMessage(service.createMimeMessage());
		
		message.setFrom("rajkumar.mardubudi@aspiresys.com");
		message.setSubject("Heelooooo");
		message.setTo(new String [] {"rajkumar.mardubudi@aspiresys.com"});
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("url", "some url");
		map.put("otp", OTP.generate());
		
		message.setVelocity("/templates/dummy.vm", map);
		
		service.send(message, NotificationType.String);
		
	}
	
	
	@Test
	public void testClickTellSmsService() throws MessagingException {
		
		NotificationService<ClickaTellSmsMessage> service = NotificationsFactory.getService(ClickaTellServiceImpl.class);
		service.buildConfiguration("test/clickatell-sms.xml");
		ClickaTellSmsMessage message = new ClickaTellSmsMessage();
		message.setTo("+918056703314");
		message.setMessage(new DefaultMessage("Hiiiiiiiiiiiii Abc"));
		service.send(message, NotificationType.String);
	}

	@Test
    public void testFcm() throws Exception {
		NotificationService<FcmMessage> service = NotificationsFactory.getService(FcmNotificationService.class);
		service.buildConfiguration("test/FcmConfig.xml");
		FcmMessage message = new FcmMessage();
//		message.setTo("ABC");
		List<String> topics = new ArrayList<String>();
		topics.add("news");
		topics.add("funny");
		topics.add("test3");
		message.setTopics(topics);
		Map<String, String> data = new HashMap<String, String>();
		data.put("body", "test");
		data.put("title", "test msg");
		message.setData(data);
		String id = null;
		try {
		  id = service.send(message, NotificationType.OTP);
		} catch (Exception ex) {
	      System.out.println(ex);
		}
		System.out.println("*** "+id);
	}
	
}
