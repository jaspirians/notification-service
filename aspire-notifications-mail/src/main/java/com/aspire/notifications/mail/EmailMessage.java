/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.notifications.mail;

import com.aspires.notifications.core.NotificationMessage;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.junit.Assert;

import java.io.File;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.FileTypeMap;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;

/**
 * Helper class for populating a {@link javax.mail.internet.MimeMessage}.
 *
 * <p>Offers support for HTML text content, inline elements such as images, and typical
 * mail attachments. Also supports personal names that accompany mail addresses.
 *
 *@author rajkumar.mardubudi
 */ 
public class EmailMessage extends NotificationMessage {

	/**
	 * Constant indicating a non-multipart message.
	 */
	public static final int MULTIPART_MODE_NO = 0;

	/**
	 * Constant indicating a multipart message with a single root multipart
	 * element of type "mixed". Texts, inline elements and attachements
	 * will all get added to that root element.
	 */
	public static final int MULTIPART_MODE_MIXED = 1;
	
	/**
	 * Constant indicating a multipart message with a single root multipart
	 * element of type "related". Texts, inline elements and attachements
	 * will all get added to that root element.
	 */
	public static final int MULTIPART_MODE_RELATED = 2;

	/**
	 * Constant indicating a multipart message with a root multipart element
	 * "mixed" plus a nested multipart element of type "related". Texts and
	 * inline elements will get added to the nested "related" element,
	 * while attachments will get added to the "mixed" root element.
	 */
	public static final int MULTIPART_MODE_MIXED_RELATED = 3;


	private static final String MULTIPART_SUBTYPE_MIXED = "mixed";

	private static final String MULTIPART_SUBTYPE_RELATED = "related";

	private static final String MULTIPART_SUBTYPE_ALTERNATIVE = "alternative";

	private static final String CONTENT_TYPE_ALTERNATIVE = "text/alternative";

	private static final String CONTENT_TYPE_HTML = "text/html";

	private static final String CONTENT_TYPE_CHARSET_SUFFIX = ";charset=";

	private static final String HEADER_PRIORITY = "X-Priority";

	private static final String HEADER_CONTENT_ID = "Content-ID";


	private  MimeMessage mimeMessage;

	private MimeMultipart rootMimeMultipart;

	private MimeMultipart mimeMultipart;

	private String encoding;

	private FileTypeMap fileTypeMap;

	private boolean validateAddresses = false;

	
	public EmailMessage() {
		
	}
	

	/**
	 * Create a new MimeMessageHelper for the given MimeMessage,
	 * assuming a simple text message (no multipart content,
	 * i.e. no alternative texts and no inline elements or attachments).
	 * <p>The character encoding for the message will be taken from
	 * the passed-in MimeMessage object, if carried there. Else,
	 * JavaMail's default encoding will be used.
	 * @param mimeMessage MimeMessage to work on
	 * @see #MimeMessageHelper(javax.mail.internet.MimeMessage, boolean)
	 * @see #getDefaultEncoding(javax.mail.internet.MimeMessage)
	 * @see JavaMailSenderImpl#setDefaultEncoding
	 */
	public EmailMessage(MimeMessage mimeMessage) {
		this(mimeMessage, null);
	}

	/**
	 * Create a new MimeMessageHelper for the given MimeMessage,
	 * assuming a simple text message (no multipart content,
	 * i.e. no alternative texts and no inline elements or attachments).
	 * @param mimeMessage MimeMessage to work on
	 * @param encoding the character encoding to use for the message
	 * @see #MimeMessageHelper(javax.mail.internet.MimeMessage, boolean)
	 */
	public EmailMessage(MimeMessage mimeMessage, String encoding) {
		this.mimeMessage = mimeMessage;
		this.encoding = (encoding != null ? encoding : getDefaultEncoding(mimeMessage));
		this.fileTypeMap = getDefaultFileTypeMap(mimeMessage);
	}

	/**
	 * Create a new MimeMessageHelper for the given MimeMessage,
	 * in multipart mode (supporting alternative texts, inline
	 * elements and attachments) if requested.
	 * <p>Consider using the MimeMessageHelper constructor that
	 * takes a multipartMode argument to choose a specific multipart
	 * mode other than MULTIPART_MODE_MIXED_RELATED.
	 * <p>The character encoding for the message will be taken from
	 * the passed-in MimeMessage object, if carried there. Else,
	 * JavaMail's default encoding will be used.
	 * @param mimeMessage MimeMessage to work on
	 * @param multipart whether to create a multipart message that
	 * supports alternative texts, inline elements and attachments
	 * (corresponds to MULTIPART_MODE_MIXED_RELATED)
	 * @throws MessagingException if multipart creation failed
	 * @see #MimeMessageHelper(javax.mail.internet.MimeMessage, int)
	 * @see #getDefaultEncoding(javax.mail.internet.MimeMessage)
	 * @see JavaMailSenderImpl#setDefaultEncoding
	 */
	public EmailMessage(MimeMessage mimeMessage, boolean multipart) throws MessagingException {
		this(mimeMessage, multipart, null);
	}

	/**
	 * Create a new MimeMessageHelper for the given MimeMessage,
	 * in multipart mode (supporting alternative texts, inline
	 * elements and attachments) if requested.
	 * <p>Consider using the MimeMessageHelper constructor that
	 * takes a multipartMode argument to choose a specific multipart
	 * mode other than MULTIPART_MODE_MIXED_RELATED.
	 * @param mimeMessage MimeMessage to work on
	 * @param multipart whether to create a multipart message that
	 * supports alternative texts, inline elements and attachments
	 * (corresponds to MULTIPART_MODE_MIXED_RELATED)
	 * @param encoding the character encoding to use for the message
	 * @throws MessagingException if multipart creation failed
	 * @see #MimeMessageHelper(javax.mail.internet.MimeMessage, int, String)
	 */
	public EmailMessage(MimeMessage mimeMessage, boolean multipart, String encoding)
			throws MessagingException {

		this(mimeMessage, (multipart ? MULTIPART_MODE_MIXED_RELATED : MULTIPART_MODE_NO), encoding);
	}

	/**
	 * Create a new MimeMessageHelper for the given MimeMessage,
	 * in multipart mode (supporting alternative texts, inline
	 * elements and attachments) if requested.
	 * <p>The character encoding for the message will be taken from
	 * the passed-in MimeMessage object, if carried there. Else,
	 * JavaMail's default encoding will be used.
	 * @param mimeMessage MimeMessage to work on
	 * @param multipartMode which kind of multipart message to create
	 * (MIXED, RELATED, MIXED_RELATED, or NO)
	 * @throws MessagingException if multipart creation failed
	 * @see #MULTIPART_MODE_NO
	 * @see #MULTIPART_MODE_MIXED
	 * @see #MULTIPART_MODE_RELATED
	 * @see #MULTIPART_MODE_MIXED_RELATED
	 * @see #getDefaultEncoding(javax.mail.internet.MimeMessage)
	 * @see JavaMailSenderImpl#setDefaultEncoding
	 */
	public EmailMessage(MimeMessage mimeMessage, int multipartMode) throws MessagingException {
		this(mimeMessage, multipartMode, null);
	}

	/**
	 * Create a new MimeMessageHelper for the given MimeMessage,
	 * in multipart mode (supporting alternative texts, inline
	 * elements and attachments) if requested.
	 * @param mimeMessage MimeMessage to work on
	 * @param multipartMode which kind of multipart message to create
	 * (MIXED, RELATED, MIXED_RELATED, or NO)
	 * @param encoding the character encoding to use for the message
	 * @throws MessagingException if multipart creation failed
	 * @see #MULTIPART_MODE_NO
	 * @see #MULTIPART_MODE_MIXED
	 * @see #MULTIPART_MODE_RELATED
	 * @see #MULTIPART_MODE_MIXED_RELATED
	 */
	public EmailMessage(MimeMessage mimeMessage, int multipartMode, String encoding)
			throws MessagingException {

		this.mimeMessage = mimeMessage;
		createMimeMultiparts(mimeMessage, multipartMode);
		this.encoding = (encoding != null ? encoding : getDefaultEncoding(mimeMessage));
		this.fileTypeMap = getDefaultFileTypeMap(mimeMessage);
	}

	
	public void setMimeMessage(MimeMessage mimeMessage) {
		this.mimeMessage = mimeMessage;
	}

	/**
	 * Return the underlying MimeMessage object.
	 */
	public final MimeMessage getMimeMessage() {
		return this.mimeMessage;
	}


	/**
	 * Determine the MimeMultipart objects to use, which will be used
	 * to store attachments on the one hand and text(s) and inline elements
	 * on the other hand.
	 * <p>Texts and inline elements can either be stored in the root element
	 * itself (MULTIPART_MODE_MIXED, MULTIPART_MODE_RELATED) or in a nested element
	 * rather than the root element directly (MULTIPART_MODE_MIXED_RELATED).
	 * <p>By default, the root MimeMultipart element will be of type "mixed"
	 * (MULTIPART_MODE_MIXED) or "related" (MULTIPART_MODE_RELATED).
	 * The main multipart element will either be added as nested element of
	 * type "related" (MULTIPART_MODE_MIXED_RELATED) or be identical to the root
	 * element itself (MULTIPART_MODE_MIXED, MULTIPART_MODE_RELATED).
	 * @param mimeMessage the MimeMessage object to add the root MimeMultipart
	 * object to
	 * @param multipartMode the multipart mode, as passed into the constructor
	 * (MIXED, RELATED, MIXED_RELATED, or NO)
	 * @throws MessagingException if multipart creation failed
	 * @see #setMimeMultiparts
	 * @see #MULTIPART_MODE_NO
	 * @see #MULTIPART_MODE_MIXED
	 * @see #MULTIPART_MODE_RELATED
	 * @see #MULTIPART_MODE_MIXED_RELATED
	 */
	protected void createMimeMultiparts(MimeMessage mimeMessage, int multipartMode) throws MessagingException {
		switch (multipartMode) {
			case MULTIPART_MODE_NO:
				setMimeMultiparts(null, null);
				break;
			case MULTIPART_MODE_MIXED:
				MimeMultipart mixedMultipart = new MimeMultipart(MULTIPART_SUBTYPE_MIXED);
				mimeMessage.setContent(mixedMultipart);
				setMimeMultiparts(mixedMultipart, mixedMultipart);
				break;
			case MULTIPART_MODE_RELATED:
				MimeMultipart relatedMultipart = new MimeMultipart(MULTIPART_SUBTYPE_RELATED);
				mimeMessage.setContent(relatedMultipart);
				setMimeMultiparts(relatedMultipart, relatedMultipart);
				break;
			case MULTIPART_MODE_MIXED_RELATED:
				MimeMultipart rootMixedMultipart = new MimeMultipart(MULTIPART_SUBTYPE_MIXED);
				mimeMessage.setContent(rootMixedMultipart);
				MimeMultipart nestedRelatedMultipart = new MimeMultipart(MULTIPART_SUBTYPE_RELATED);
				MimeBodyPart relatedBodyPart = new MimeBodyPart();
				relatedBodyPart.setContent(nestedRelatedMultipart);
				rootMixedMultipart.addBodyPart(relatedBodyPart);
				setMimeMultiparts(rootMixedMultipart, nestedRelatedMultipart);
				break;
			default:
				throw new IllegalArgumentException("Only multipart modes MIXED_RELATED, RELATED and NO supported");
		}
	}

	/**
	 * Set the given MimeMultipart objects for use by this MimeMessageHelper.
	 * @param root the root MimeMultipart object, which attachments will be added to;
	 * or {@code null} to indicate no multipart at all
	 * @param main the main MimeMultipart object, which text(s) and inline elements
	 * will be added to (can be the same as the root multipart object, or an element
	 * nested underneath the root multipart element)
	 */
	protected final void setMimeMultiparts(MimeMultipart root, MimeMultipart main) {
		this.rootMimeMultipart = root;
		this.mimeMultipart = main;
	}

	/**
	 * Return whether this helper is in multipart mode,
	 * i.e. whether it holds a multipart message.
	 * @see #MimeMessageHelper(MimeMessage, boolean)
	 */
	public final boolean isMultipart() {
		return (this.rootMimeMultipart != null);
	}

	/**
	 * Throw an IllegalStateException if this helper is not in multipart mode.
	 */
	private void checkMultipart() throws IllegalStateException {
		if (!isMultipart()) {
			throw new IllegalStateException("Not in multipart mode - " +
				"create an appropriate MimeMessageHelper via a constructor that takes a 'multipart' flag " +
				"if you need to set alternative texts or add inline elements or attachments.");
		}
	}

	/**
	 * Return the root MIME "multipart/mixed" object, if any.
	 * Can be used to manually add attachments.
	 * <p>This will be the direct content of the MimeMessage,
	 * in case of a multipart mail.
	 * @throws IllegalStateException if this helper is not in multipart mode
	 * @see #isMultipart
	 * @see #getMimeMessage
	 * @see javax.mail.internet.MimeMultipart#addBodyPart
	 */
	public final MimeMultipart getRootMimeMultipart() throws IllegalStateException {
		checkMultipart();
		return this.rootMimeMultipart;
	}

	/**
	 * Return the underlying MIME "multipart/related" object, if any.
	 * Can be used to manually add body parts, inline elements, etc.
	 * <p>This will be nested within the root MimeMultipart,
	 * in case of a multipart mail.
	 * @throws IllegalStateException if this helper is not in multipart mode
	 * @see #isMultipart
	 * @see #getRootMimeMultipart
	 * @see javax.mail.internet.MimeMultipart#addBodyPart
	 */
	public final MimeMultipart getMimeMultipart() throws IllegalStateException {
		checkMultipart();
		return this.mimeMultipart;
	}


	/**
	 * Determine the default encoding for the given MimeMessage.
	 * @param mimeMessage the passed-in MimeMessage
	 * @return the default encoding associated with the MimeMessage,
	 * or {@code null} if none found
	 */
	protected String getDefaultEncoding(MimeMessage mimeMessage) {
		if (mimeMessage instanceof SmartMimeMessage) {
			return ((SmartMimeMessage) mimeMessage).getDefaultEncoding();
		}
		return null;
	}

	/**
	 * Return the specific character encoding used for this message, if any.
	 */
	public String getEncoding() {
		return this.encoding;
	}

	/**
	 * Determine the default Java Activation FileTypeMap for the given MimeMessage.
	 * @param mimeMessage the passed-in MimeMessage
	 * @return the default FileTypeMap associated with the MimeMessage,
	 * or a default ConfigurableMimeFileTypeMap if none found for the message
	 * @see ConfigurableMimeFileTypeMap
	 */
	protected FileTypeMap getDefaultFileTypeMap(MimeMessage mimeMessage) {
		if (mimeMessage instanceof SmartMimeMessage) {
			FileTypeMap fileTypeMap = ((SmartMimeMessage) mimeMessage).getDefaultFileTypeMap();
			if (fileTypeMap != null) {
				return fileTypeMap;
			}
		}
		return fileTypeMap;
	}

	
	
	/**
	 * Set the Java Activation Framework {@code FileTypeMap} to use
	 * for determining the content type of inline content and attachments
	 * that get added to the message.
	 * <p>Default is the {@code FileTypeMap} that the underlying
	 * MimeMessage carries, if any, or the Activation Framework's default
	 * {@code FileTypeMap} instance else.
	 * @see #addInline
	 * @see #addAttachment
	 * @see #getDefaultFileTypeMap(javax.mail.internet.MimeMessage)
	 * @see JavaMailSenderImpl#setDefaultFileTypeMap
	 * @see javax.activation.FileTypeMap#getDefaultFileTypeMap
	 * @see ConfigurableMimeFileTypeMap
	 */
	public void setFileTypeMap(FileTypeMap fileTypeMap) {
		this.fileTypeMap = (fileTypeMap != null ? fileTypeMap : getDefaultFileTypeMap(getMimeMessage()));
	}

	/**
	 * Return the {@code FileTypeMap} used by this MimeMessageHelper.
	 */
	public FileTypeMap getFileTypeMap() {
		return this.fileTypeMap;
	}


	/**
	 * Set whether to validate all addresses which get passed to this helper.
	 * Default is "false".
	 * <p>Note that this is by default just available for JavaMail >= 1.3.
	 * You can override the default {@code validateAddress method} for
	 * validation on older JavaMail versions (or for custom validation).
	 * @see #validateAddress
	 */
	public void setValidateAddresses(boolean validateAddresses) {
		this.validateAddresses = validateAddresses;
	}

	/**
	 * Return whether this helper will validate all addresses passed to it.
	 */
	public boolean isValidateAddresses() {
		return this.validateAddresses;
	}

	/**
	 * Validate the given mail address.
	 * Called by all of MimeMessageHelper's address setters and adders.
	 * <p>Default implementation invokes {@code InternetAddress.validate()},
	 * provided that address validation is activated for the helper instance.
	 * <p>Note that this method will just work on JavaMail >= 1.3. You can override
	 * it for validation on older JavaMail versions or for custom validation.
	 * @param address the address to validate
	 * @throws AddressException if validation failed
	 * @see #isValidateAddresses()
	 * @see javax.mail.internet.InternetAddress#validate()
	 */
	protected void validateAddress(InternetAddress address) throws AddressException {
		if (isValidateAddresses()) {
			address.validate();
		}
	}

	/**
	 * Validate all given mail addresses.
	 * Default implementation simply delegates to validateAddress for each address.
	 * @param addresses the addresses to validate
	 * @throws AddressException if validation failed
	 * @see #validateAddress(InternetAddress)
	 */
	protected void validateAddresses(InternetAddress[] addresses) throws AddressException {
		for (InternetAddress address : addresses) {
			validateAddress(address);
		}
	}


	public void setFrom(InternetAddress from) throws MessagingException {
		Assert.assertNotEquals(from, "From address must not be null");
		validateAddress(from);
		this.mimeMessage.setFrom(from);
	}

	public void setFrom(String from)  {
		Assert.assertNotEquals(from, "From address must not be null");
		try {
			setFrom(parseAddress(from));
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setFrom(String from, String personal) throws MessagingException, UnsupportedEncodingException {
		Assert.assertNotEquals(from, "From address must not be null");
		setFrom(getEncoding() != null ?
			new InternetAddress(from, personal, getEncoding()) : new InternetAddress(from, personal));
	}

	public void setReplyTo(InternetAddress replyTo) throws MessagingException {
		Assert.assertNotEquals(replyTo, "Reply-to address must not be null");
		validateAddress(replyTo);
		this.mimeMessage.setReplyTo(new InternetAddress[] {replyTo});
	}

	public void setReplyTo(String replyTo) throws MessagingException {
		Assert.assertNotEquals(replyTo, "Reply-to address must not be null");
		setReplyTo(parseAddress(replyTo));
	}

	public void setReplyTo(String replyTo, String personal) throws MessagingException, UnsupportedEncodingException {
		Assert.assertNotEquals(replyTo, "Reply-to address must not be null");
		InternetAddress replyToAddress = (getEncoding() != null) ?
				new InternetAddress(replyTo, personal, getEncoding()) : new InternetAddress(replyTo, personal);
		setReplyTo(replyToAddress);
	}


	public void setTo(InternetAddress to) throws MessagingException {
		Assert.assertNotEquals(to, "To address must not be null");
		validateAddress(to);
		this.mimeMessage.setRecipient(Message.RecipientType.TO, to);
	}

	public void setTo(InternetAddress[] to) throws MessagingException {
		Assert.assertNotEquals(to, "To address array must not be null");
		validateAddresses(to);
		this.mimeMessage.setRecipients(Message.RecipientType.TO, to);
	}

	public void setTo(String to) {
		Assert.assertNotEquals(to, "To address must not be null");
		try {
			setTo(parseAddress(to));
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setTo(String[] to) throws MessagingException {
		Assert.assertNotEquals(to, "To address array must not be null");
		InternetAddress[] addresses = new InternetAddress[to.length];
		for (int i = 0; i < to.length; i++) {
			addresses[i] = parseAddress(to[i]);
		}
		setTo(addresses);
	}

	public void addTo(InternetAddress to) throws MessagingException {
		Assert.assertNotEquals(to, "To address must not be null");
		validateAddress(to);
		this.mimeMessage.addRecipient(Message.RecipientType.TO, to);
	}

	public void addTo(String to) throws MessagingException {
		Assert.assertNotEquals(to, "To address must not be null");
		addTo(parseAddress(to));
	}

	public void addTo(String to, String personal) throws MessagingException, UnsupportedEncodingException {
		Assert.assertNotEquals(to, "To address must not be null");
		addTo(getEncoding() != null ?
			new InternetAddress(to, personal, getEncoding()) :
			new InternetAddress(to, personal));
	}


	public void setCc(InternetAddress cc) throws MessagingException {
		Assert.assertNotEquals(cc, "Cc address must not be null");
		validateAddress(cc);
		this.mimeMessage.setRecipient(Message.RecipientType.CC, cc);
	}

	public void setCc(InternetAddress[] cc) throws MessagingException {
		Assert.assertNotEquals(cc, "Cc address array must not be null");
		validateAddresses(cc);
		this.mimeMessage.setRecipients(Message.RecipientType.CC, cc);
	}

	public void setCc(String cc) throws MessagingException {
		Assert.assertNotEquals(cc, "Cc address must not be null");
		setCc(parseAddress(cc));
	}

	public void setCc(String[] cc) throws MessagingException {
		Assert.assertNotEquals(cc, "Cc address array must not be null");
		InternetAddress[] addresses = new InternetAddress[cc.length];
		for (int i = 0; i < cc.length; i++) {
			addresses[i] = parseAddress(cc[i]);
		}
		setCc(addresses);
	}

	public void addCc(InternetAddress cc) throws MessagingException {
		Assert.assertNotEquals(cc, "Cc address must not be null");
		validateAddress(cc);
		this.mimeMessage.addRecipient(Message.RecipientType.CC, cc);
	}

	public void addCc(String cc) throws MessagingException {
		Assert.assertNotEquals(cc, "Cc address must not be null");
		addCc(parseAddress(cc));
	}

	public void addCc(String cc, String personal) throws MessagingException, UnsupportedEncodingException {
		Assert.assertNotEquals(cc, "Cc address must not be null");
		addCc(getEncoding() != null ?
			new InternetAddress(cc, personal, getEncoding()) :
			new InternetAddress(cc, personal));
	}


	public void setBcc(InternetAddress bcc) throws MessagingException {
		Assert.assertNotEquals(bcc, "Bcc address must not be null");
		validateAddress(bcc);
		this.mimeMessage.setRecipient(Message.RecipientType.BCC, bcc);
	}

	public void setBcc(InternetAddress[] bcc) throws MessagingException {
		Assert.assertNotEquals(bcc, "Bcc address array must not be null");
		validateAddresses(bcc);
		this.mimeMessage.setRecipients(Message.RecipientType.BCC, bcc);
	}

	public void setBcc(String bcc) throws MessagingException {
		Assert.assertNotEquals(bcc, "Bcc address must not be null");
		setBcc(parseAddress(bcc));
	}

	public void setBcc(String[] bcc) throws MessagingException {
		Assert.assertNotEquals(bcc, "Bcc address array must not be null");
		InternetAddress[] addresses = new InternetAddress[bcc.length];
		for (int i = 0; i < bcc.length; i++) {
			addresses[i] = parseAddress(bcc[i]);
		}
		setBcc(addresses);
	}

	public void addBcc(InternetAddress bcc) throws MessagingException {
		Assert.assertNotEquals(bcc, "Bcc address must not be null");
		validateAddress(bcc);
		this.mimeMessage.addRecipient(Message.RecipientType.BCC, bcc);
	}

	public void addBcc(String bcc) throws MessagingException {
		Assert.assertNotEquals(bcc, "Bcc address must not be null");
		addBcc(parseAddress(bcc));
	}

	public void addBcc(String bcc, String personal) throws MessagingException, UnsupportedEncodingException {
		Assert.assertNotEquals(bcc, "Bcc address must not be null");
		addBcc(getEncoding() != null ?
			new InternetAddress(bcc, personal, getEncoding()) :
			new InternetAddress(bcc, personal));
	}

	private InternetAddress parseAddress(String address) throws MessagingException {
		InternetAddress[] parsed = InternetAddress.parse(address);
		if (parsed.length != 1) {
			throw new AddressException("Illegal address", address);
		}
		InternetAddress raw = parsed[0];
		try {
			return (getEncoding() != null ?
					new InternetAddress(raw.getAddress(), raw.getPersonal(), getEncoding()) : raw);
		}
		catch (UnsupportedEncodingException ex) {
			throw new MessagingException("Failed to parse embedded personal name to correct encoding", ex);
		}
	}


	/**
	 * Set the priority ("X-Priority" header) of the message.
	 * @param priority the priority value;
	 * typically between 1 (highest) and 5 (lowest)
	 * @throws MessagingException in case of errors
	 */
	public void setPriority(int priority) throws MessagingException {
		this.mimeMessage.setHeader(HEADER_PRIORITY, Integer.toString(priority));
	}

	/**
	 * Set the sent-date of the message.
	 * @param sentDate the date to set (never {@code null})
	 * @throws MessagingException in case of errors
	 */
	public void setSentDate(Date sentDate) throws MessagingException {
		Assert.assertNotEquals(sentDate, "Sent date must not be null");
		this.mimeMessage.setSentDate(sentDate);
	}

	/**
	 * Set the subject of the message, using the correct encoding.
	 * @param subject the subject text
	 * @throws MessagingException in case of errors
	 */
	public void setSubject(String subject) throws MessagingException {
		Assert.assertNotEquals(subject, "Subject must not be null");
		if (getEncoding() != null) {
			this.mimeMessage.setSubject(subject, getEncoding());
		}
		else {
			this.mimeMessage.setSubject(subject);
		}
	}


	/**
	 * Set the given text directly as content in non-multipart mode
	 * or as default body part in multipart mode.
	 * Always applies the default content type "text/plain".
	 * <p><b>NOTE:</b> Invoke {@link #addInline} <i>after</i> {@code setText};
	 * else, mail readers might not be able to resolve inline references correctly.
	 * @param text the text for the message
	 * @throws MessagingException in case of errors
	 */
	public void setText(String text) throws MessagingException {
		setText(text, false);
	}

	/**
	 * Set the given text directly as content in non-multipart mode
	 * or as default body part in multipart mode.
	 * The "html" flag determines the content type to apply.
	 * <p><b>NOTE:</b> Invoke {@link #addInline} <i>after</i> {@code setText};
	 * else, mail readers might not be able to resolve inline references correctly.
	 * @param text the text for the message
	 * @param html whether to apply content type "text/html" for an
	 * HTML mail, using default content type ("text/plain") else
	 * @throws MessagingException in case of errors
	 */
	public void setText(String text, boolean html) throws MessagingException {
		Assert.assertNotEquals(text, "Text must not be null");
		MimePart partToUse;
		if (isMultipart()) {
			partToUse = getMainPart();
		}
		else {
			partToUse = this.mimeMessage;
		}
		if (html) {
			setHtmlTextToMimePart(partToUse, text);
		}
		else {
			setPlainTextToMimePart(partToUse, text);
		}
	}

	  public void setVelocity(String path, Map<String, Object> inputData) throws MessagingException
	  {
	    Assert.assertNotEquals(path, "Path must not be null");
	    VelocityEngine engine = new VelocityEngine();
	    VelocityContext context = new VelocityContext(inputData);
	    StringWriter result = new StringWriter();
	    Template template = engine.getTemplate(path);
	    template.merge(context, result);
	    setText(result.toString(), true);
	  }
	
	
	/**
	 * Set the given plain text and HTML text as alternatives, offering
	 * both options to the email client. Requires multipart mode.
	 * <p><b>NOTE:</b> Invoke {@link #addInline} <i>after</i> {@code setText};
	 * else, mail readers might not be able to resolve inline references correctly.
	 * @param plainText the plain text for the message
	 * @param htmlText the HTML text for the message
	 * @throws MessagingException in case of errors
	 */
	public void setText(String plainText, String htmlText) throws MessagingException {
		Assert.assertNotEquals(plainText, "Plain text must not be null");
		Assert.assertNotEquals(htmlText, "HTML text must not be null");

		MimeMultipart messageBody = new MimeMultipart(MULTIPART_SUBTYPE_ALTERNATIVE);
		getMainPart().setContent(messageBody, CONTENT_TYPE_ALTERNATIVE);

		// Create the plain text part of the message.
		MimeBodyPart plainTextPart = new MimeBodyPart();
		setPlainTextToMimePart(plainTextPart, plainText);
		messageBody.addBodyPart(plainTextPart);

		// Create the HTML text part of the message.
		MimeBodyPart htmlTextPart = new MimeBodyPart();
		setHtmlTextToMimePart(htmlTextPart, htmlText);
		messageBody.addBodyPart(htmlTextPart);
	}

	private MimeBodyPart getMainPart() throws MessagingException {
		MimeMultipart mimeMultipart = getMimeMultipart();
		MimeBodyPart bodyPart = null;
		for (int i = 0; i < mimeMultipart.getCount(); i++) {
			BodyPart bp = mimeMultipart.getBodyPart(i);
			if (bp.getFileName() == null) {
				bodyPart = (MimeBodyPart) bp;
			}
		}
		if (bodyPart == null) {
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeMultipart.addBodyPart(mimeBodyPart);
			bodyPart = mimeBodyPart;
		}
		return bodyPart;
	}

	private void setPlainTextToMimePart(MimePart mimePart, String text) throws MessagingException {
		if (getEncoding() != null) {
			mimePart.setText(text, getEncoding());
		}
		else {
			mimePart.setText(text);
		}
	}

	private void setHtmlTextToMimePart(MimePart mimePart, String text) throws MessagingException {
		if (getEncoding() != null) {
			mimePart.setContent(text, CONTENT_TYPE_HTML + CONTENT_TYPE_CHARSET_SUFFIX + getEncoding());
		}
		else {
			mimePart.setContent(text, CONTENT_TYPE_HTML);
		}
	}


	/**
	 * Add an inline element to the MimeMessage, taking the content from a
	 * {@code javax.activation.DataSource}.
	 * <p>Note that the InputStream returned by the DataSource implementation
	 * needs to be a <i>fresh one on each call</i>, as JavaMail will invoke
	 * {@code getInputStream()} multiple times.
	 */
	public void addInline(String contentId, DataSource dataSource) throws MessagingException {
		Assert.assertNotEquals(contentId, "Content ID must not be null");
		Assert.assertNotEquals(dataSource, "DataSource must not be null");
		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		mimeBodyPart.setDisposition(MimeBodyPart.INLINE);
		// We're using setHeader here to remain compatible with JavaMail 1.2,
		// rather than JavaMail 1.3's setContentID.
		mimeBodyPart.setHeader(HEADER_CONTENT_ID, "<" + contentId + ">");
		mimeBodyPart.setDataHandler(new DataHandler(dataSource));
		getMimeMultipart().addBodyPart(mimeBodyPart);
	}

	/**
	 * Add an inline element to the MimeMessage, taking the content from a
	 * {@code java.io.File}.
	 */
	public void addInline(String contentId, File file) throws MessagingException {
		Assert.assertNotEquals(file, "File must not be null");
		FileDataSource dataSource = new FileDataSource(file);
		dataSource.setFileTypeMap(getFileTypeMap());
		addInline(contentId, dataSource);
	}
	
}
