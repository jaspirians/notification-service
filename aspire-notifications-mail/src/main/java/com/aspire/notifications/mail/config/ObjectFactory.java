/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.notifications.mail.config;

import com.aspires.notifications.core.ValidatedDocument;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory implements ValidatedDocument {

    private final static QName _EmailProvider_QNAME = new QName("", "emailProvider");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.aspire
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EmailProvider }
     * 
     */
    public EmailProvider createEmailProvider() {
        return new EmailProvider();
    }

    /**
     * Create an instance of {@link Entry }
     * 
     */
    public Entry createEntry() {
        return new Entry();
    }

    /**
     * Create an instance of {@link Properties }
     * 
     */
    public Properties createProperties() {
        return new Properties();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailProvider }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "emailProvider")
    public JAXBElement<EmailProvider> createEmailProvider(EmailProvider value) {
        return new JAXBElement<EmailProvider>(_EmailProvider_QNAME, EmailProvider.class, null, value);
    }

}
