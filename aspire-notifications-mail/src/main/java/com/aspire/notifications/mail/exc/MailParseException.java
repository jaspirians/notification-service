/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.notifications.mail.exc;

import com.aspires.notifications.exc.NotificationException;

/**
 * Exception thrown if illegal message properties are encountered.
 * 
 * @author rajkumar.mardubudi
 */
@SuppressWarnings("serial")
public class MailParseException extends NotificationException {

	/**
	 * Constructor for MailParseException.
	 * @param msg the detail message
	 */
	public MailParseException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for MailParseException.
	 * @param msg the detail message
	 * @param cause the root cause from the mail API in use
	 */
	public MailParseException(String msg, Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Constructor for MailParseException.
	 * @param cause the root cause from the mail API in use
	 */
	public MailParseException(Throwable cause) {
		super("Could not parse mail", cause);
	}

}
