/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.notifications.mail.service;

import com.aspire.notifications.mail.EmailMessage;
import com.aspires.notifications.core.NotificationService;

import javax.mail.internet.MimeMessage;

/**
 Clients should talk to the mail sender through this interface if they need
 * mail functionality.
 * The production implementation is {@link EmailServiceImpl};
 * 
 * @author rajkumar.mardubudi
*/

public interface EmailService extends NotificationService<EmailMessage> {
	public MimeMessage createMimeMessage();
}
