/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.notifications.mail.service;

import com.aspire.notifications.mail.EmailMessage;
import com.aspire.notifications.mail.SmartMimeMessage;
import com.aspire.notifications.mail.config.EmailProvider;
import com.aspire.notifications.mail.config.Entry;
import com.aspire.notifications.mail.config.ObjectFactory;
import com.aspire.notifications.mail.config.Properties;
import com.aspire.notifications.mail.exc.MailSendException;
import com.aspires.notifications.core.AbstractNotificationService;
import com.aspires.notifications.core.NotificationType;
import com.aspires.notifications.exc.NotificationException;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

/**
 * Production implementation of the {@link EmailService} interface
 * 
 * @author rajkumar.mardubudi
 */

public class EmailServiceImpl extends AbstractNotificationService<EmailProvider, EmailMessage>  implements EmailService {
	
	@Override
	public String send(EmailMessage message, NotificationType notificationType) throws NotificationException {
		MimeMessage[] mimeMessage = {message.getMimeMessage()};
		 doSend(mimeMessage, null);
		return null;
	}
	
	private static final String HEADER_MESSAGE_ID = "Message-ID";
	
	public EmailServiceImpl() {
		super("config/emailConfig.xsd", EmailProvider.class, ObjectFactory.class);
	}
	
	
	/**
	 * Actually send the given array of MimeMessages via JavaMail.
	 * 
	 * @param mimeMessages
	 *            MimeMessage objects to send
	 * @param originalMessages
	 *            corresponding original message objects that the MimeMessages
	 *            have been created from (with same array length and indices as
	 *            the "mimeMessages" array), if any
	 */
	protected void doSend(MimeMessage[] mimeMessages, Object[] originalMessages)
			throws NotificationException {
		Map<Object, Exception> failedMessages = new LinkedHashMap<Object, Exception>();
		Transport transport = null;

		try {
			for (int i = 0; i < mimeMessages.length; i++) {

				// Check transport connection first...
				if (transport == null || !transport.isConnected()) {
					if (transport != null) {
						try {
							transport.close();
						} catch (Exception ex) {
							// Ignore - we're reconnecting anyway
						}
						transport = null;
					}
					try {
						transport = connectTransport();
					} catch (Exception ex) {
						// Effectively, all remaining messages failed...
						for (int j = i; j < mimeMessages.length; j++) {
							Object original = (originalMessages != null ? originalMessages[j] : mimeMessages[j]);
							failedMessages.put(original, ex);
						}
						throw new MailSendException("Mail server connection failed", ex, failedMessages);
					}
				}

				// Send message via current transport...
				MimeMessage mimeMessage = mimeMessages[i];
				try {
					if (mimeMessage.getSentDate() == null) {
						mimeMessage.setSentDate(new Date());
					}
					String messageId = mimeMessage.getMessageID();
					mimeMessage.saveChanges();
					if (messageId != null) {
						// Preserve explicitly specified message id...
						mimeMessage.setHeader(HEADER_MESSAGE_ID, messageId);
					}
					transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
				} catch (Exception ex) {
					Object original = (originalMessages != null ? originalMessages[i] : mimeMessage);
					failedMessages.put(original, ex);
				}
			}
		} finally {
			try {
				if (transport != null) {
					transport.close();
				}
			} catch (Exception ex) {
				if (!failedMessages.isEmpty()) {
					throw new MailSendException("Failed to close server connection after message failures", ex, failedMessages);
				} else {
					throw new MailSendException("Failed to close server connection after message sending", ex);
				}
			}
		}

		if (!failedMessages.isEmpty()) {
			throw new MailSendException(failedMessages);
		}
	}
	
	/**
	 * Obtain and connect a Transport from the underlying JavaMail Session,
	 * passing in the specified host, port, username, and password.
	 * 
	 * @return the connected Transport object
	 * @throws MessagingException
	 *             if the connect attempt failed
	 * @throws javax.mail.MessagingException
	 * @throws NumberFormatException
	 * @throws NoSuchProviderException
	 */
	protected Transport connectTransport()
			throws MessagingException, NumberFormatException, javax.mail.MessagingException, NoSuchProviderException {
		Transport transport = getTransport(getSession(provider.getProperties()), provider.getProtocol());
		transport.connect(provider.getHost(), provider.getPort(), provider.getUsername(),
				provider.getPassword());
		return transport;
	}
	
	/**
	 * Obtain a Transport object from the given JavaMail Session, using the
	 * configured protocol.
	 * <p>
	 * Can be overridden in subclasses, e.g. to return a mock Transport object.
	 * 
	 * @throws javax.mail.NoSuchProviderException
	 */
	protected Transport getTransport(Session session, String protocol) throws NoSuchProviderException {
		return session.getTransport(protocol);
	}
	
	/**
	 * Return the JavaMail {@code Session}, lazily initializing it if hasn't
	 * been specified explicitly.
	 */
	public synchronized Session getSession(Properties properties) {
		java.util.Properties props = new java.util.Properties();
		List<Entry> entries = properties.getEntry();
		for (Entry entry : entries) {
			props.setProperty(entry.getKey(), entry.getValue());
		}
		return Session.getInstance(props);
	}
	
	/**
	 * Create a new JavaMail MimeMessage for the underlying JavaMail Session
	 * of this sender, using the given input stream as the message source.
	 */
	public MimeMessage createMimeMessage() {
		return new SmartMimeMessage(getSession(provider.getProperties()), null, null);
	}
}
