/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.notifications.push.fcm.exc;

import com.aspires.notifications.exc.NotificationException;

/**
 * Created by deepak.daniel on 2/7/16.
 */
public class FcmException extends NotificationException {
  public FcmException(String message) {
	super(message);
  }
	
  public FcmException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
