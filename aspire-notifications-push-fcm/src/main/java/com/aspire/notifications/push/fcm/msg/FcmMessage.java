/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.notifications.push.fcm.msg;

import java.util.List;
import java.util.Map;

import com.aspires.notifications.core.NotificationMessage;

/**
 * Created by deepak.daniel on 12/6/16.
 */
public class FcmMessage extends NotificationMessage{

  private String to;
  private List<String> condition;
  private Map<String, String> data;
  private Map<String, String> notification;

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public List<String> getTopics() {
  	return condition;
  }
  
  public void setTopics(List<String> topics) {
  	this.condition = topics;
  }

  public Map<String, String> getData() {
  	return data;
  }
  
  public void setData(Map<String, String> data) {
  	this.data = data;
  }
  
  public Map<String, String> getNotification() {
  	return notification;
  }
  
  public void setNotification(Map<String, String> notification) {
  	this.notification = notification;
  }
}
