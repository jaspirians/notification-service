/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.notifications.push.fcm.service;

import com.aspire.notifications.push.fcm.config.FcmProvider;
import com.aspire.notifications.push.fcm.config.ObjectFactory;
import com.aspire.notifications.push.fcm.exc.FcmException;
import com.aspire.notifications.push.fcm.msg.FcmMessage;
import com.aspire.restapi.RestClient;
import com.aspires.notifications.core.AbstractNotificationService;
import com.aspires.notifications.core.NotificationType;
import com.aspires.notifications.exc.NotificationException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * Created by deepak.daniel on 12/6/16.
 */
public class FcmNotificationService extends AbstractNotificationService<FcmProvider, FcmMessage> {
  public static final String FCM_URL = "https://fcm.googleapis.com";

  public FcmNotificationService() {
    super("config/FcmConfig.xsd", FcmProvider.class, ObjectFactory.class);
  }

  @Override
  public String send(FcmMessage message, NotificationType notificationType) throws NotificationException {
    String messageId = null;
    
    validateMessage(message);
    
    RestClient.RestClientBuilder restClientBuilder = new RestClient.RestClientBuilder(FCM_URL)
        .addHeaderElement("Authorization", "key="+provider.getServerKey());
    RestClient restClient = restClientBuilder.build();
    Map<String, String> params = new HashMap<String, String>();
    String body = constructMessage(message);
    try {
      Response response = restClient.post("/fcm/send", body, params);
      if(response.getStatus() == Status.OK.getStatusCode()) {
        messageId = response.readEntity(String.class);
      } else {
    	throw new FcmException("Error while sending message to FCM Server: "+response.toString());
      }
    } catch(FcmException fEx) {
      throw fEx;
    } catch (Exception ex) {
      throw new FcmException("Unexpected exception in FCM Provider: ", ex);
    }
    return messageId;
  }
  
  private void validateMessage(FcmMessage message) throws FcmException {
	String to = message.getTo();
	List<String> topics = message.getTopics();
	if (to != null && !to.equals("")
		  && topics != null && !topics.isEmpty()) {
	  throw new FcmException("Validation error: must not set both 'to' field and 'topics' field");
	}
	if (to != null && to.equals("")
		  || topics != null && topics.isEmpty()) {
	  throw new FcmException("Validation error: must set either 'to' field or 'topics' field");
	}
	if (topics != null && topics.size() > 3) {
      throw new FcmException("Validation error: only maximum of three topics are allowed");
	}
  }
  
  private String constructMessage(FcmMessage message) {
	StringBuilder body = new StringBuilder();
	body.append("{");
	body.append(constructRecipient(message));
	if(message.getData() != null && message.getData().size() > 0) {
	  body.append(",");
	  body.append("\"data\":"+constructMap(message.getData()));
	}
	if(message.getNotification() != null && message.getNotification().size() > 0) {
      body.append(",");
	  body.append("\"notification\":"+constructMap(message.getNotification()));
	}
	body.append("}");
	return body.toString();
  }
  
  private String constructRecipient(FcmMessage message) {
	StringBuilder recipient = new StringBuilder();
	String to = message.getTo();
	List<String> topics = message.getTopics();
	if(to != null && !to.equals("")) {
	  recipient.append("\"to\":\""+to+"\"");
	} else if (topics != null && !topics.isEmpty()) {
	  if(topics.size() == 1) {
		recipient.append("\"to\":\"/topics/"+topics.get(0)+"\"");
	  } else {
	    recipient.append("\"condition\":\"");
	    Iterator<String> topicIterator = topics.iterator(); 
	    while(topicIterator.hasNext()) {
	      String topic = topicIterator.next();
	  	  recipient.append("'"+topic+"'"+" in topics");
	  	  if(topicIterator.hasNext()) {
	  		recipient.append(" || ");
	  	  }
	    }
	    recipient.append("\"");
	  }
	}
	return recipient.toString();
  }
  
  private String constructMap(Map<String, String> maps) {
	StringBuilder mapString = new StringBuilder();
	mapString.append("{");
	Iterator<Map.Entry<String, String>> mapIterator = maps.entrySet().iterator();
	while(mapIterator.hasNext()) {
	  Map.Entry<String, String> map = mapIterator.next();
	  mapString.append("\""+map.getKey()+"\":"+"\""+map.getValue()+"\"");
	  if(mapIterator.hasNext()) {
		  mapString.append(",");
	  }
	}
	mapString.append("}");
	return mapString.toString();
  }
}