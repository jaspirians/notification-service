/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.notifications.sms.clickatell.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clickaTellProvider", propOrder = {
    "apiId",
    "username",
    "password"
})
public class ClickaTellProvider {

  @XmlElement(required = true)	
  private String apiId;
  
  @XmlElement(required = true)
  private String username;
  
  @XmlElement(required = true)
  private String password;
  
  public String getApiId() {
    return apiId;
  }
  
  public void setApiId(String apiId) {
    this.apiId = apiId;
  }
  
  public String getUsername() {
    return username;
  }
  
  public void setUsername(String username) {
    this.username = username;
  }
  
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  
  
  
}
