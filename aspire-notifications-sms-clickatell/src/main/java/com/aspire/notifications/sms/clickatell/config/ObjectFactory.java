/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.notifications.sms.clickatell.config;

import com.aspires.notifications.core.ValidatedDocument;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory implements ValidatedDocument {

    private final static QName _ClickaTellProvider_QNAME = new QName("", "ClickaTellProvider");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.aspire
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TwilioProvider }
     * 
     */
    public ClickaTellProvider createTwilioProvider() {
        return new ClickaTellProvider();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TwilioProvider }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "clickaTellProvider")
    public JAXBElement<ClickaTellProvider> createClickaTellProvider(ClickaTellProvider value) {
        return new JAXBElement<ClickaTellProvider>(_ClickaTellProvider_QNAME, ClickaTellProvider.class, null, value);
    }

}
