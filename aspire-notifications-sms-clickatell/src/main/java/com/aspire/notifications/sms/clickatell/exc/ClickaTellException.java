/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.notifications.sms.clickatell.exc;

import com.aspires.notifications.exc.NotificationException;

@SuppressWarnings("serial")
public class ClickaTellException extends NotificationException{

  public ClickaTellException(String msg) {
    super(msg);
  }
  
  public ClickaTellException(String msg, Throwable cause){
    super(msg,cause);
  }

}
