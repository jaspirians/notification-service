/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.notifications.sms.clickatell.service;

import com.aspire.notifications.sms.clickatell.config.ClickaTellProvider;
import com.aspire.notifications.sms.clickatell.config.ObjectFactory;
import com.aspire.notifications.sms.clickatell.exc.ClickaTellException;
import com.aspire.notifications.sms.clickatell.msg.ClickaTellSmsMessage;
import com.aspire.restapi.RestClient;
import com.aspires.notifications.core.AbstractNotificationService;
import com.aspires.notifications.core.NotificationType;
import com.aspires.notifications.exc.NotificationException;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class ClickaTellServiceImpl
    extends AbstractNotificationService<ClickaTellProvider, ClickaTellSmsMessage> {

  public ClickaTellServiceImpl() {
    super("config/ClickTellSmsConfig.xsd", ClickaTellProvider.class, ObjectFactory.class);
  }

  @Override
  public String send(ClickaTellSmsMessage message, NotificationType notificationType)
      throws NotificationException {
    return doSend(message);
  }

  private String doSend(ClickaTellSmsMessage message) {
    Map<String, String> params = new HashMap<String, String>();
    String messageId = null;
    try {
    	
    	System.setProperty("java.net.useSystemProxies", "true");
        System.setProperty("http.proxyHost", "proxy.aspiresys.com");
        System.setProperty("http.proxyPort", "3128");
	
      params.put("user", provider.getUsername());
      params.put("password", provider.getPassword());
      params.put("api_id", provider.getApiId());
      params.put("to", message.getTo());
      params.put("text", message.getMessage().getMessage());

      RestClient client = new RestClient.RestClientBuilder("http://api.clickatell.com").build();
      Response response = client.post("/http/sendmsg", null, params);
      
      if(response.getStatus() == Status.OK.ordinal()) {
    	  messageId = response.readEntity(String.class);
      }
      
    } catch (Exception e) {
      throw new ClickaTellException("Exception in sending sms via ClickaTell service: ", e);
    }
    return messageId;
  }

}
