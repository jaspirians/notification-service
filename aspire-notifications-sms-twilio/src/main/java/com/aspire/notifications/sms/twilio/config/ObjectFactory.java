
package com.aspire.notifications.sms.twilio.config;

import com.aspires.notifications.core.ValidatedDocument;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory implements ValidatedDocument {

    private final static QName _TwilioProvider_QNAME = new QName("", "TwilioProvider");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.aspire
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TwilioProvider }
     * 
     */
    public TwilioProvider createTwilioProvider() {
        return new TwilioProvider();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TwilioProvider }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "twilioProvider")
    public JAXBElement<TwilioProvider> createTwilioProvider(TwilioProvider value) {
        return new JAXBElement<TwilioProvider>(_TwilioProvider_QNAME, TwilioProvider.class, null, value);
    }

}
