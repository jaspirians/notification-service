package com.aspire.notifications.sms.twilio.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "twilioProvider", propOrder = {
    "accountSid",
    "authToken",
    "callerId"
})
public class TwilioProvider {

	@XmlElement(required = true)
	private String accountSid;
	
	@XmlElement(required = true)
	private String authToken;
	
	@XmlElement(required = true)
	private String callerId;

	public String getAccountSid() {
		return accountSid;
	}

	public void setAccountSid(String accountSid) {
		this.accountSid = accountSid;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getCallerId() {
		return callerId;
	}

	public void setCallerId(String callerId) {
		this.callerId = callerId;
	}
	
}
