package com.aspire.notifications.sms.twilio.exc;

import com.aspires.notifications.exc.NotificationException;

@SuppressWarnings("serial")
public class TwilioException extends NotificationException{

  public TwilioException(String msg) {
    super(msg);
  }
  
  public TwilioException(String msg, Throwable cause){
    super(msg,cause);
  }

}
