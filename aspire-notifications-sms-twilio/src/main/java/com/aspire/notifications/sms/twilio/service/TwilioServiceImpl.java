package com.aspire.notifications.sms.twilio.service;

import com.aspire.notifications.sms.twilio.config.ObjectFactory;
import com.aspire.notifications.sms.twilio.config.TwilioProvider;
import com.aspire.notifications.sms.twilio.exc.TwilioException;
import com.aspire.notifications.sms.twilio.msg.SmsMessage;
import com.aspires.notifications.core.AbstractNotificationService;
import com.aspires.notifications.core.NotificationType;
import com.aspires.notifications.exc.InvalidInputException;
import com.aspires.notifications.exc.NotificationException;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class TwilioServiceImpl extends AbstractNotificationService<TwilioProvider, SmsMessage>{

  private TwilioRestClient twilioRestClient;
  
  public TwilioServiceImpl() {
    super("config/twiloSmsConfig.xsd", TwilioProvider.class, ObjectFactory.class);
  }
  
  
  @Override
  public void buildConfiguration(String xml) {
    super.buildConfiguration(xml);
    twilioRestClient = new TwilioRestClient(provider.getAccountSid(), provider.getAuthToken());
  }

  @Override
    public void buildConfiguration(String inputXml, Class documentClass)
            throws InvalidInputException
    {
       super.buildConfiguration(inputXml, documentClass);
       twilioRestClient = new TwilioRestClient(provider.getAccountSid(), provider.getAuthToken());
    }
  
  @Override
  public String send(SmsMessage message, NotificationType notificationType)
      throws NotificationException {
    return doSend(message);
  }

  private String doSend(SmsMessage message) {
    String messageId = null;
    try {
      List<NameValuePair> params = new ArrayList<NameValuePair>();
      params.add(new BasicNameValuePair("To", message.getTo()));
      params.add(new BasicNameValuePair("From", provider.getCallerId()));
      params.add(new BasicNameValuePair("Body", message.getMessage().getMessage()));
      MessageFactory messageFactory = twilioRestClient.getAccount().getMessageFactory();
      Message newmessage = messageFactory.create(params);
      messageId = newmessage.getSid();
    } catch (TwilioRestException e) {
        e.printStackTrace();
      throw new TwilioException("Exception in sending sms via Twilio service: ", e);
    }
    return messageId;
  }

}
