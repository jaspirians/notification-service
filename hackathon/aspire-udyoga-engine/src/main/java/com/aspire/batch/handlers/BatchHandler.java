/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.batch.handlers;

import com.aspire.commons.engine.exe.ServiceException;
import com.aspire.commons.entities.BatchInstance;

/**
 * Handles all batch related operations
 * @author rajkumar.mardubudi
 *
 */
public interface BatchHandler
{
    /**
     * Executes batch
     * @param instance
     */
    public void execute(BatchInstance instance) throws ServiceException;
    
}
