/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.adaptors;

import com.aspire.commons.engine.adaptors.req.AdapterRequest;
import com.aspire.commons.engine.adaptors.res.AdapterResponse;

/**
 * This is a base Adapter interface. It is used to expose complete functional
 * endpoints to the external parties and provide last layer of functional
 * isolation from runtime environment.
 * 
 * @author rajkumar.mardubudi
 */
public interface Adapter
{
    /**
     * validate the request implementation
     * 
     * @param t
     * @param y
     * @return
     */
    public <T extends AdapterRequest, Y extends AdapterResponse> void validate(
            T t, Y y) throws Exception;
}
