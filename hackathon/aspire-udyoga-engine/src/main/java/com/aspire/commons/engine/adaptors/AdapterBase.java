/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.adaptors;

import com.aspire.commons.engine.adaptors.core.ComplexException;
import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.adaptors.req.AdapterRequest;
import com.aspire.commons.engine.adaptors.res.AdapterResponse;
import com.aspire.commons.engine.services.ServiceFactory;
import com.aspire.commons.logs.RLogFactory;

import org.apache.commons.logging.Log;

import java.util.ArrayList;
import java.util.List;

import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;

/**
 * Define address specific adapter
 * 
 * @see Adapter
 * 
 * @author rajkumar.mardubudi
 * 
 */
public abstract class AdapterBase implements Adapter
{
    
    protected static final Log log = RLogFactory.getLog(AdapterBase.class);
    protected static final MessageBundle bundle = new MessageBundle(
            AdapterBase.class);
    protected static Validator validator = new Validator();
    protected ServiceFactory serviceFactory = new ServiceFactory();
    
    /**
     * 
     * @return {@link ServiceFactory}
     * 
     *         public ServiceFactory getServiceFactory() { return
     *         EngineContext.getContext().getServiceFactory(); }
     */
    
    /**
     * Called by subclasses to validate request object
     * 
     * @param t
     * @return
     */
    public <T extends AdapterRequest, Y extends AdapterResponse> void validate(
            T t, Y y) throws Exception
    {
        List<ConstraintViolation> violations = validator.validate(t);
        if (violations.size() > 0)
        {
            ArrayList<ComplexMessage> errors = new ArrayList<ComplexMessage>();
            for (ConstraintViolation v : violations)
            {
                ComplexMessage m = new ComplexMessage("E_Validation", bundle,
                        v.getErrorCode(), v.getCheckName(), v.getMessage());
                y.getErrors().add(m);
            }
            throw new ComplexException(new ComplexMessage("E_ValidationFailed",
                    bundle, t.getClass()));
        }
    }
}
