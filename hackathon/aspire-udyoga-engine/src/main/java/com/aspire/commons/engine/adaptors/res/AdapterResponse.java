/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.adaptors.res;

import com.aspire.commons.engine.adaptors.core.ComplexMessage;
import com.aspire.commons.engine.adaptors.core.Status;

import java.util.List;

/**
 * Adapter request object
 */
public interface AdapterResponse
{
    /**
     * @return {@link Status} code
     */
    public String getVersion();
    
    /**
     * @return {@link Status} code
     */
    public Status getStatus();
    
    /**
     * @return {@link Long} time in ms of how long the call took
     */
    public long getTime();
    
    /**
     * @return
     */
    public List<ComplexMessage> getErrors();
}
