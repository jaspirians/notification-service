/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.engine.dao;

import com.aspire.commons.entities.AbstractEntity;
import com.aspire.commons.utils.HibernateUtil;

import org.hibernate.Criteria;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * 
 */
public abstract class AbstractDAO<T extends AbstractEntity>
{
    
    private Class<T> entityClass;
    
    public AbstractDAO(Class<T> clazz)
    {
        entityClass = clazz;
    }
    
    public List<T> findAll()
    {
        @SuppressWarnings("unchecked")
        List<T> list = getSession().createCriteria(entityClass)
                .list();
        return list;
    }
    
    public ScrollableResults fetchIdsForIndexing(Session session,
            String companyId)
    {
        String className = entityClass.getClass().getName();
        String baseQuery = String.format("select id from %s", className);
        
        String query = null;
        if (companyId == null)
        {
            query = baseQuery;
        }
        else
        {
            query = String.format("%s where company_id = '%s'", baseQuery,
                    companyId);
        }
        
        ScrollableResults ids = session.createQuery(query).setReadOnly(true)
                .setFetchSize(Integer.MIN_VALUE)
                .scroll(ScrollMode.FORWARD_ONLY);
        return ids;
    }
    
    public List<T> findBatchByIds(List<String> ids)
    {
        @SuppressWarnings("unchecked")
        List<T> list = getSession()
                .createCriteria(entityClass.getClass())
                .add(Restrictions.in("id", ids)).list();
        return list;
    }
    
    public T load(String id)
    {
        T entity = (T) getSession().load(entityClass, id);
        return entity;
    }
    
    public T findById(String id)
    {
        @SuppressWarnings("unchecked")
        T entity = (T) getSession().get(entityClass, id);
        
        return entity;
    }
    
    public T save(T entity)
    {
        getSession().save(entity);
        return entity;
    }
    
    public T update(T entity)
    {
        getSession().update(entity);
        return entity;
    }
    
    public void makeTransient(T entity)
    {
        getSession().delete(entity);
    }
    
    public void forget(T entity)
    {
        getSession().evict(entity);
    }
    
    /**
     * Use this inside subclasses as a convenience method.
     */
    @SuppressWarnings("unchecked")
    protected List<T> findByCriteria(Criterion... criterion)
    {
        Criteria crit = getSession()
                .createCriteria(entityClass.getClass());
        for (Criterion c : criterion)
        {
            crit.add(c);
        }
        return crit.list();
    }
    
    protected SessionFactory getSessionFactory()
    {
        return HibernateUtil.getSessionFactory();
    }
    
    protected Session getSession()
    {
        return getSessionFactory().getCurrentSession();
    }
}
