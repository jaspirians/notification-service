/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.dao;

import com.aspire.commons.entities.Batch;

/**
 * 
 * @author rajkumar.mardubudi
 *
 */
public class BatchDao extends AbstractDAO<Batch>
{
    public BatchDao()
    {
        super(Batch.class);
    }
}
