/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.dao;

import com.aspire.commons.entities.BatchInstance;

/**
 * 
 * @author rajkumar.mardubudi
 *
 */
public class BatchInstanceDao extends AbstractDAO<BatchInstance>
{
    public BatchInstanceDao()
    {
        super(BatchInstance.class);
    }
}
