/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.dao;

public class DaoFactory
{
    private static BatchDao batchDao = new BatchDao();
    private static BatchInstanceDao batchInstanceDao = new BatchInstanceDao();
    
    
    /**
     * @return the batchDao
     */
    public static BatchDao getBatchDao()
    {
        return batchDao;
    }
    
    /**
     * @return the batchDao
     */
    public static BatchInstanceDao getBatchInstanceDao()
    {
        return batchInstanceDao;
    }
    
    
    
}
