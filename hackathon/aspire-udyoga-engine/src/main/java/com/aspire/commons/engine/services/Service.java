/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.services;

import com.aspire.commons.engine.adaptors.core.MessageBundle;
import com.aspire.commons.engine.dao.DaoFactory;
import com.aspire.commons.engine.exe.ServiceException;
import com.aspire.commons.entities.AbstractEntity;
import com.aspire.commons.logs.RLogFactory;
import com.aspire.commons.utils.HibernateUtil;

import org.apache.commons.logging.Log;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.Collection;

public class Service
{
    protected static final Log log = RLogFactory.getLog(Service.class);
    protected static final MessageBundle bundle = new MessageBundle(Service.class);
    
    
    
    protected static DaoFactory factory_ = new DaoFactory();
    
    private static final Service INSTANCE = new Service();
    
    protected static final Service getInstance()
    {
        return INSTANCE;
    }
    
    public static void start() throws ServiceException
    {
        HibernateUtil.begin();
    }
    
    public static void success()
    {
        HibernateUtil.commit();
    }
    
    protected static org.hibernate.Session getSession()
    {
        return HibernateUtil.getSession();
    }
    
    protected static void clear()
    {
        HibernateUtil.getSession().clear();
    }
    
    protected static void evict(AbstractEntity e)
    {
        HibernateUtil.getSession().evict(e);
    }
    
    protected static void refresh(AbstractEntity e)
    {
        HibernateUtil.getSession().refresh(e);
    }
    
    public static void fail(Throwable t)
    {
        if (t != null)
        {
            if (t instanceof OutOfMemoryError)
            {
                log.error(" ********* Critical alert *********");
            }
            
            String title = "failure";
            StackTraceElement[] trace = Thread.currentThread().getStackTrace();
            if (trace != null && trace.length > 0)
            {
                title = trace[0].toString();
            }
            log.error(title, t);
        }
        
        Transaction txn = getSession().getTransaction();
        if (txn != null) {
            HibernateUtil.rollback();
        }
    }
    
    public static void end()
    {
        HibernateUtil.closeSession();
    }
    
    protected static AbstractEntity merge(AbstractEntity ge)
    {
        return (AbstractEntity) HibernateUtil.getSession().merge(ge);
    }
    
    protected static void makePersistent(AbstractEntity ge)
    {
        HibernateUtil.getSession().saveOrUpdate(ge);
    }
    
    protected static void deleteQueuedEntity(AbstractEntity ge)
    {
        HibernateUtil.getSession().delete(ge);
    }
    
    protected static void persistCollection(Collection<AbstractEntity> set)
    {
        for (AbstractEntity entity : set)
        {
            makePersistent(entity);
        }
    }
    
}
