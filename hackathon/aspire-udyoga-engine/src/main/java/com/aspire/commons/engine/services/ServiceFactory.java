/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.engine.services;

public class ServiceFactory
{
    
    private static BatchService batchService;
    
    public static BatchService getBatchService()
    {
        return batchService;
    }
    
    {
    	batchService = new BatchService();
    }
}
