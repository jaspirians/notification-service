package com.aspire.commons.engine.servlet;

import com.aspire.commons.engine.services.BatchService;

import javax.servlet.ServletException;

public class UdyogaServletContainer extends org.glassfish.jersey.servlet.ServletContainer
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 100000000L;

	@Override
	public void init() throws ServletException {
		super.init();
		new BatchService();
	}
	
}
