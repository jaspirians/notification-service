/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.entities;

public class AbstractEntity
{
    
    private String id;
    
    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }
    
    /**
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
}
