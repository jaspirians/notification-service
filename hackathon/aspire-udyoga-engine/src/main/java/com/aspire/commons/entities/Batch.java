/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.entities;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author rajkumar.mardubudi
 *
 */
@XmlRootElement
public class Batch extends AbstractEntity
{
    
    protected String name;
    protected String description;
    protected String trigger;
    protected String jobClass;
    protected String handle;
    protected String parameters;
    protected int _active;
    
    /**
     * @return the _active
     */
    @XmlTransient
    public int get_active()
    {
        return _active;
    }
    /**
     * @param _active the _active to set
     */
    public void set_active(int _active)
    {
        this._active = _active;
    }
    /**
    
    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }
    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }
    /**
     * @return the trigger
     */
    public String getTrigger()
    {
        return trigger;
    }
    /**
     * @param trigger the trigger to set
     */
    public void setTrigger(String trigger)
    {
        this.trigger = trigger;
    }
    /**
     * @return the jobClass
     */
    public String getJobClass()
    {
        return jobClass;
    }
    /**
     * @param jobClass the jobClass to set
     */
    public void setJobClass(String jobClass)
    {
        this.jobClass = jobClass;
    }
    /**
     * @return the handle
     */
    public String getHandle()
    {
        return handle;
    }
    /**
     * @param handle the handle to set
     */
    public void setHandle(String handle)
    {
        this.handle = handle;
    }
    /**
     * @return the parameters
     */
    public String getParameters()
    {
        return parameters;
    }
    /**
     * @param parameters the parameters to set
     */
    public void setParameters(String parameters)
    {
        this.parameters = parameters;
    }
    /**
     * @return the active
     */
    public boolean isActive()
    {
        return this._active != 0;
    }
    /**
     * @param active the active to set
     */
    public void setActive(boolean _active)
    {
        this._active = _active ? 1 : 0;
    }
    
    
    
    
}
