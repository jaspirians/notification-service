/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.entities;

import com.aspire.commons.engine.adaptors.core.Status;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @see BatchInstance
 */
@XmlRootElement
public class BatchInstance extends AbstractEntity
{
	protected Batch batch;
	/**
	 */
	protected Date startTime;
	/**
	 */
	protected Date stopTime;
	/**
	 */
	protected Date nextTime;
	/**
	 */
	protected String errorMessage;
	/**
	 */
	protected Status statusCode;
	
	protected int processed = 0;
	
	protected int failed = 0;

	/**
	 * @return the batch
	 */
	@XmlElement(type=Batch.class)
	public Batch getBatch()
	{
		return batch;
	}

	/**
	 * @param batch the batch to set
	 */
	public void setBatch(Batch batch)
	{
		this.batch = batch;
	}

	/**
	 * @return the startTime
	 */
	public Date getStartTime()
	{
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Date startTime)
	{
		this.startTime = startTime;
	}

	/**
	 * @return the stopTime
	 */
	public Date getStopTime()
	{
		return stopTime;
	}

	/**
	 * @param stopTime the stopTime to set
	 */
	public void setStopTime(Date stopTime)
	{
		this.stopTime = stopTime;
	}

	/**
	 * @return the nextTime
	 */
	public Date getNextTime()
	{
		return nextTime;
	}

	/**
	 * @param nextTime the nextTime to set
	 */
	public void setNextTime(Date nextTime)
	{
		this.nextTime = nextTime;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the statusCode
	 */
	public Status getStatusCode()
	{
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(Status statusCode)
	{
		this.statusCode = statusCode;
	}

	/**
	 * @return the processed
	 */
	public int getProcessed()
	{
		return processed;
	}

	/**
	 * @param processed the processed to set
	 */
	public void setProcessed(int processed)
	{
		this.processed = processed;
	}

	/**
	 * @return the failed
	 */
	public int getFailed()
	{
		return failed;
	}

	/**
	 * @param failed the failed to set
	 */
	public void setFailed(int failed)
	{
		this.failed = failed;
	}
	
}
