/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */

package com.aspire.commons.entities;

import java.util.List;

public class Employee {

	private String empId;
	private String firstName;
	private String lastName;
	private String headline;
	private String summary;
	private long lastModified;
	private String proposalComments;
	private String associations;
	private String interests;
	private List<Experience> experiences;
	private List<Organization> organizations;
	private List<Publication> publications;
	private List<Skill> skills; 
	private List<Education> educations;
	private PersonalDetails personalDetails;
	private HonorsAndAwards honorsAndAwards;
	private List<Language> languages;
	private List<Patent> patents;
	private List<Certification> certifications;
	private List<Course> courses;
	private List<Volunteer> volunteers;
	
	
	
	
	
	
	
	
}
