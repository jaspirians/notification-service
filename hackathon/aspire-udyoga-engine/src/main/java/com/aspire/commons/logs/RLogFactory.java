/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.logs;

import com.aspire.commons.engine.adaptors.core.MessageBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.MDC;

import java.util.Locale;

/**
 * @see RLog
 */
public class RLogFactory
{
    
    /**
     * Context Variable.
     * 
     * @param name
     * @param value
     */
    public static void addContextVariable(String name, Object value)
    {
        MDC.put(name, value);
    }
    
    /**
     * Get log for specifc class
     * 
     * @param clazz
     * @return
     */
    public static Log getLog(Class<?> clazz)
    {
        return getLog(clazz, Locale.getDefault());
    }
    
    /**
     * Get log for specific class
     * 
     * @param clazz
     * @return
     */
    public static Log getLog(Class<?> clazz, Locale locale)
    {
        MessageBundle rb = new MessageBundle(clazz, locale);
        Log clog = LogFactory.getLog(clazz);
        return new RLog(clog, rb);
    }
    
    /**
     * Get log for specific class
     * 
     * @param clazz
     * @return
     */
    public static Log getLog(String logger, Locale locale)
    {
        MessageBundle rb = new MessageBundle(RLogFactory.class, locale);
        Log clog = LogFactory.getLog(logger);
        return new RLog(clog, rb);
    }
    
    /**
     * Get log for specific class
     * 
     * @param clazz
     * @return
     */
    public static Log getLog(String logger)
    {
        MessageBundle rb = new MessageBundle(RLogFactory.class);
        Log clog = LogFactory.getLog(logger);
        return new RLog(clog, rb);
    }
    
    /**
     * @param clazz
     *            the class
     * @param resourceBundle
     *            the bundle to use
     * @return log for the specified class priming it with the specified
     *         resource bundle
     */
    public static Log getLog(Class<?> clazz, MessageBundle resourceBundle)
    {
        return new RLog(LogFactory.getLog(clazz), resourceBundle);
    }
}
