
package com.aspire.commons.utils;

import com.aspire.commons.engine.exe.ServiceException;

import org.hibernate.FlushMode;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class HibernateUtil
{
    
    final static Logger logger = LoggerFactory.getLogger(HibernateUtil.class);
    
    private static class SessionFactoryHolder
    {
        public static final SessionFactory FACTORY = buildFactory();
        
        private static SessionFactory buildFactory()
        {
            try
            {
                logger.info("configuring notification database session factory");
                return new Configuration().configure().buildSessionFactory();
            }
            catch (Throwable t)
            {
                logger.error(t.getMessage(), t);
                throw new ExceptionInInitializerError(t);
            }
        }
    }
    
    public static SessionFactory getSessionFactory()
    {
        return SessionFactoryHolder.FACTORY;
    }
    
    public static org.hibernate.Session getSession()
    {
        return SessionFactoryHolder.FACTORY.getCurrentSession();
    }
    
    public static final Transaction begin() throws ServiceException
    {
        return getSession().beginTransaction();
    }
    
    public static final void commit()
    {
        Transaction txn = getSession().getTransaction();
        if (txn != null)
        {
            txn.commit();
        }
    }
    
    public static final void rollback()
    {
        Transaction txn = getSession().getTransaction();
        if (txn != null)
        {
            txn.rollback();
        }
    }
    
    public static final void closeSession()
    {
        getSession().close();
    }
    
    public static void flush()
    {
        getSession().flush();
    }
    
    public static FlushMode getFlushMode()
    {
        return getSession().getFlushMode();
    }
    
    public static void setFlushMode(FlushMode mode)
    {
        getSession().setFlushMode(mode);
    }
}
