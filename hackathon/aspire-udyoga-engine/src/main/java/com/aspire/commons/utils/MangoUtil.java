package com.aspire.commons.utils;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;

public class MangoUtil 
{

	public static void main(String[] args) throws UnknownHostException {
		
		
		//MongoClient mongoClient = new MongoClient("ds019916.mlab.com", 19916);
		
		MongoClient mongoClient = new MongoClient("172.24.144.139", 27017);
		
		
		//String dbURI = "mongodb://hackathon_123:hackathon#123@ds019916.mlab.com:19916/";
		//MongoClient mongoClient = new MongoClient(new MongoClientURI(dbURI));
		
		
		System.out.println(mongoClient.getDatabaseNames());
		
		DB db = mongoClient.getDB("epb");
		
		//boolean flag = db.authenticate("hackathon_123", "hackathon#123".toCharArray());
		
		//System.out.println(flag);
		
		DBCollection collection = db.getCollection("dummyColl");

		// 1. BasicDBObject example
		System.out.println("BasicDBObject example...");
		BasicDBObject document = new BasicDBObject();
		document.put("database", "mkyongDB");
		document.put("table", "hosting");

		BasicDBObject documentDetail = new BasicDBObject();
		documentDetail.put("records", 99);
		documentDetail.put("index", "vps_index1");
		documentDetail.put("active", "true");
		document.put("detail", documentDetail);
		
		

		collection.insert(document);
		
		DBCursor cursorDoc = collection.find();
		while (cursorDoc.hasNext()) {
			System.out.println(cursorDoc.next());
		}

		
		
	}
}
