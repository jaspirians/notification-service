/**
 * Copyright Aspire Systems Pvt Ltd 2016
 */
package com.aspire.commons.utils.jaxb;

import java.util.ArrayList;
import java.util.Set;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.annotation.XmlRootElement;

import org.reflections.Reflections;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;

/**
 * {@link JAXBRegistry} {@link ContextResolver} from JAXRS and Jersey
 * 
 * @author rajkumar.mardubudi
 */
@Provider
public class JAXBRegistry extends JacksonJaxbJsonProvider
{
    
    private static ObjectMapper mapper = new ObjectMapper();
    
    static
    {
        long x = System.currentTimeMillis();
        Reflections reflections = new Reflections("com.aspire");
        Set<Class<?>> cls = reflections
                .getTypesAnnotatedWith(XmlRootElement.class);
        final ArrayList<Class<?>> noNullList = new ArrayList<Class<?>>();
        noNullList.addAll(cls);
        cls.clear();
        cls.addAll(noNullList);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                false);
        mapper.setVisibility(PropertyAccessor.ALL,
                JsonAutoDetect.Visibility.ANY);
        mapper.setAnnotationIntrospector(
                new JaxbAnnotationIntrospector(mapper.getTypeFactory()));
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.registerSubtypes(noNullList.toArray(new Class<?>[0]));
    }
    
    public JAXBRegistry()
    {
        super();
        setMapper(mapper);
    }
}
